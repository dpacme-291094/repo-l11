C queries

+A listing of managers sorted by the number of events that they organise.
select m from Manager m order by m.events.size desc;

+A listing of managers that includes the amount that they due in fees.
select m from Manager m;

+A listing of chorbies sorted by the number of events to which they have registered.
select c from Chorbi c order by c.events.size desc;

+A listing of chorbies that includes the amount that they due in fees.
select c from Chorbi c;

B Queries

+The minimum, the maximum, and the average number of stars per chorbi.
///*select l.likesRecipient,min(l.rate)*1.0,max(l.rate)*1.0,floor(abs(avg(l.rate)) * 100 + 0.5)/100.0 * sign(avg(l.rate)) from Likes l group by l.likesRecipient;*/

Maximum:
select l.likesRecipient,l.rate*1.0 from Likes l where l.rate=(select max(l.rate) from Likes l);

Minimum:
select l.likesRecipient,l.rate*1.0 from Likes l where l.rate=(select min(l.rate) from Likes l);

Average:
select floor(abs(avg(l.rate)) * 100 + 0.5)/100.0 * sign(avg(l.rate)) from Likes l;



+The list of chorbies, sorted by the average number of stars that they�ve got.
select l.likesRecipient,floor(abs(avg(l.rate)) * 100 + 0.5)/100.0 * sign(avg(l.rate)) from Likes l group by l.likesRecipient order by floor(abs(avg(l.rate)) * 100 + 0.5)/100.0 * sign(avg(l.rate)) desc;

A Queries
