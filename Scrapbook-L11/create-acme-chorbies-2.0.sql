drop database if exists `Acme-Chorbies-2.0`;
create database `Acme-Chorbies-2.0`;

grant select, insert, update, delete 
	on `Acme-Chorbies-2.0`.* to 'acme-user'@'%';

grant select, insert, update, delete, create, drop, references, index, alter, 
        create temporary tables, lock tables, create view, create routine, 
        alter routine, execute, trigger, show view
    on `Acme-Chorbies-2.0`.* to 'acme-manager'@'%';