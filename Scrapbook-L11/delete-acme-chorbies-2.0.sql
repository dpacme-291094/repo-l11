start transaction;

use `acme-chorbies-2.0`;
revoke all privileges on `acme-chorbies-2.0`.* from 'acme-user'@'%';
drop user 'acme-user'@'%';

revoke all privileges on `acme-chorbies-2.0`.* from 'acme-manager'@'%';

drop user 'acme-manager'@'%';

drop database `acme-chorbies-2.0`;
commit;