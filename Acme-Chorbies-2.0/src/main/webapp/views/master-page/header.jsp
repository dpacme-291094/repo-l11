<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<script type="text/javascript">
	function relativeRedir(loc) {
		var b = document.getElementsByTagName('base');
		if (b && b[0] && b[0].href) {
			if (b[0].href.substr(b[0].href.length - 1) == '/' && loc.charAt(0) == '/')
				loc = loc.substr(1);
			loc = b[0].href + loc;
		}
		window.location.replace(loc);
	}
</script>


<div>

	<center>
		<a href="http://localhost:8080/Acme-Chorbies-2.0/"><img
			src="images/logo.png" alt="Acme Chorbies co., Inc." /></a>
	</center>
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->

		<security:authorize access="hasRole('ADMINISTRATOR')">

			<li><a class="fNiv" href="configuration/administrator/edit.do"><spring:message
						code="master.page.configuration" /></a></li>
			<li><a class="fNiv" href="chorbi/administrator/fee.do"><spring:message
						code="master.page.pay" /></a></li>
			<li><a class="fNiv" href="chorbi/administrator/listBan.do"><spring:message
						code="master.page.ban" /></a></li>
			<li><a class="fNiv" href="chorbies/chorbi/list.do"><spring:message
						code="master.page.chorbi.list" /> </a></li>

			<li><a href="report/administrator/dashboard.do"><spring:message
						code="master.page.dashboard" /></a></li>
		</security:authorize>



		<security:authorize access="hasRole('CHORBI')">


			<li><a class="fNiv" href="chorbies/chorbi/list.do"><spring:message
						code="master.page.chorbi.list" /> </a></li>
			
			<li><a class="fNiv" href="searchTemplate/chorbi/list.do"> <spring:message
						code="master.page.search" /></a>
		</security:authorize>

		<security:authorize access="hasRole('CHORBI') || hasRole('MANAGER')">
			<li><a class="fNiv"> <spring:message
						code="master.page.chirp" /></a>
				<ul>
					<li class="arrow"></li>

					<li><a href="chirp/actor/inbox.do"><spring:message
								code="master.page.inbox" /> </a></li>
					<li><a href="chirp/actor/outbox.do"><spring:message
								code="master.page.outbox" /> </a></li>
				</ul></li>
		</security:authorize>

		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message
						code="master.page.login" /></a></li>
			<li><a class="fNiv"> <spring:message
						code="master.page.register" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="chorbi/register.do"><spring:message
								code="master.page.chorbi" /></a></li>
					<li><a href="manager/register.do"><spring:message
								code="master.page.manager" /></a></li>

				</ul></li>
		</security:authorize>

		<li><a class="fNiv"> <spring:message code="master.page.event" /></a>
			<ul>
				<li class="arrow"></li>
				<security:authorize access="hasRole('CHORBI')">
					<li><a href="event/chorbi/list.do"><spring:message
								code="master.page.event.chorbiList"></spring:message></a></li>
				</security:authorize>

				<security:authorize access="hasRole('MANAGER')">
					<li><a href="event/manager/list.do"><spring:message
								code="master.page.event.chorbiList" /></a></li>
				</security:authorize>

				<li><a href="event/browseOneMonth.do"><spring:message
							code="master.page.browseOneMonth" /></a></li>
				<li><a href="event/browseAll.do"><spring:message
							code="master.page.browseAll" /></a></li>
			</ul></li>

		<security:authorize access="isAuthenticated()">
			<li><a class="fNiv"> <spring:message
						code="master.page.profile" /> (<security:authentication
						property="principal.username" />)
			</a>
				<ul>
					<li class="arrow"></li>

					<security:authorize access="hasRole('CHORBI')">
						<li><a href="chorbi/private.do"><spring:message
									code="master.page.chorbi.profile" /> </a></li>
					</security:authorize>
					<li><a href="j_spring_security_logout"><spring:message
								code="master.page.logout" /> </a></li>
				</ul></li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>


<!--//BLOQUE COOKIES-->
<div id="barraaceptacion" style="display: block;">
	<div class="inner">
		<spring:message code="master.page.law.solicitud" />
		<a href="javascript:void(0);" class="ok" onclick="PonerCookie();"><b>OK</b></a>
		| <a href="law/cookies.do" target="_blank" class="info"><spring:message
				code="master.page.information" /></a> <a href="law/term.do"
			target="_blank" class="info"><spring:message
				code="master.page.law.term" /></a>
	</div>
</div>

<div id="cookies">
	<script>
		function getCookie(c_name) {
			var c_value = document.cookie;
			var c_start = c_value.indexOf(" " + c_name + "=");
			if (c_start == -1) {
				c_start = c_value.indexOf(c_name + "=");
			}
			if (c_start == -1) {
				c_value = null;
			} else {
				c_start = c_value.indexOf("=", c_start) + 1;
				var c_end = c_value.indexOf(";", c_start);
				if (c_end == -1) {
					c_end = c_value.length;
				}
				c_value = unescape(c_value.substring(c_start, c_end));
			}
			return c_value;
		}

		function setCookie(c_name, value, exdays) {
			var exdate = new Date();
			exdate.setDate(exdate.getDate() + exdays);
			var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
			document.cookie = c_name + "=" + c_value;
		}

		if (getCookie('tiendaaviso') != "1") {
			document.getElementById("barraaceptacion").style.display = "block";
		} else {
			document.getElementById("barraaceptacion").style.display = "none";
		}
		function PonerCookie() {
			setCookie('tiendaaviso', '1', 365);
			document.getElementById("barraaceptacion").style.display = "none";
		}
	</script>
</div>
