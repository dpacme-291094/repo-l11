<%--
 * index.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p>
	<jstl:if test="${banner!='ERROR'}">
		<img src="${banner}" style="width: 400px; height: 172px;">
	</jstl:if>
	<jstl:if test="${banner=='ERROR'}">
		<spring:message code="welcome.emptyBanner" />
	</jstl:if>
</p>
<p>
<h2>
	<spring:message code="welcome.greeting.prefix" />
</h2>
</p>

