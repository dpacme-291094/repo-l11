<%--
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="manager/register.do" modelAttribute="formManager">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount.Authorities" />
	<form:hidden path="events" />
	<form:hidden path="totalFee" />
	<form:hidden path="chirpsReceived" />
	<form:hidden path="chirpsSent" />
	<table id="formus">
		<tr>
			<td><u>
					<h3>
						<spring:message code="manager.personal" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="manager.name" path="name" />
		<acme:textbox code="manager.surname" path="surname" />
		<acme:textbox code="manager.email" path="email" />
		<acme:textbox code="manager.phone" path="phone" />

		<acme:textbox code="manager.VATNumber" path="company" />
		<acme:textbox code="manager.company" path="VATNumber" />

		<tr>
			<td><u>
					<h3>
						<spring:message code="manager.cc" />
					</h3>
			</u></td>
		</tr>

		<acme:textbox code="cc.holderName" path="creditCard.holderName" />



		<acme:selectModified code="cc.brandName" path="creditCard.brandName"
			items="${brandNames}" id="brandName" />


		<acme:textbox code="cc.expirationYear"
			path="creditCard.expirationYear" />


		<acme:textbox code="cc.expirationMonth"
			path="creditCard.expirationMonth" />

		<acme:textbox code="cc.number" path="creditCard.number" />

		<acme:textbox code="cc.codecvv" path="creditCard.codeCVV" />

		<tr>
			<td><u>
					<h3>
						<spring:message code="manager.userac" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="manager.userAccount" path="userAccount.username" />

		<acme:password code="manager.password" path="userAccount.password" />
		<tr>
			<td><b><spring:message code="manager.repassword" /></b></td>
			<td><input type="password" name="repassword" /></td>
		</tr>


	</table>

	<input type="checkbox" name="terms">

	<spring:message code="manager.terms" />
	<br />
	<br />

	<acme:submit name="save" code="manager.register" />
	<acme:cancel url="welcome/index.do" code="manager.cancel" />
</form:form>



