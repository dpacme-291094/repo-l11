<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>



<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<form:form action="searchTemplate/chorbi/edit.do"
	modelAttribute="searchTemplate">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="moment" />
	<form:hidden path="chorbi" />
	<form:hidden path="chorbies" />

	<table id="formus">
		<acme:selectModified code="searchTemplate.genre" path="genre"
			items="${genre}" id="genre" />
		<acme:selectModified code="searchTemplate.relationship"
			path="relationship" items="${relationship}" id="relationship" />
		<acme:textbox code="searchTemplate.keyword" path="keyword" />
		<acme:textbox code="searchTemplate.age" path="age" />

		<acme:textbox code="searchTemplate.country" path="coordinates.country" />
		<acme:textbox code="searchTemplate.state" path="coordinates.state" />
		<acme:textbox code="searchTemplate.province"
			path="coordinates.province" />
		<acme:textbox code="searchTemplate.city" path="coordinates.city" />

	</table>
	<acme:submit code="searchTemplate.save" name="save" />
	<acme:cancel code="searchTemplate.cancel" url="welcome/index.do" />

</form:form>

<jstl:if test="${hayChorbies != 0}">
	<display:table name="chorbies" id="row"
		requestURI="searchTemplate/chorbi/edit.do" pagesize="5"
		class="displaytag">
		<display:column titleKey="searchTemplate.chorbies.picture">
			<a href="chorbies/chorbi/profile.do?chorbiId=${row.id}"><img
				src="${row.picture}" width="100" height="120" /></a>
		</display:column>
		<display:column property="name"
			titleKey="searchTemplate.chorbies.name" sortable="false" />
		<display:column property="surname"
			titleKey="searchTemplate.chorbies.surname" sortable="false" />


		<display:column property="relationship" titleKey="chorbi.looking" />
		<display:column titleKey="chorbi.genre">


			<jstl:if test="${row.genre=='man'}">
				<spring:message code="chorbi.man" />


			</jstl:if>
			<jstl:if test="${row.genre=='woman'}">
				<spring:message code="chorbi.woman" />


			</jstl:if>
		</display:column>
	</display:table>
</jstl:if>


