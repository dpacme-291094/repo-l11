<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="chorbi/creditCard.do" modelAttribute="creditCard">

	<table id="formus">


		<acme:textbox code="cc.holderName" path="holderName" />



		<acme:selectModified code="cc.brandName" path="brandName"
			items="${brandNames}" id="brandName" />


		<acme:textbox code="cc.expirationYear" path="expirationYear" />


		<acme:textbox code="cc.expirationMonth" path="expirationMonth" />

		<acme:textbox code="cc.number" path="number" />

		<acme:textbox code="cc.codecvv" path="codeCVV" />
	</table>

	<acme:submit code="chorbi.save" name="saveCreditCard" />


	<acme:cancel url="chorbi/private.do" code="chorbi.cancel" />


</form:form>
