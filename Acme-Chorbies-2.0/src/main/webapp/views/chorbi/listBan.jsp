<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<display:table name="chorbies" id="row"
	requestURI="chorbi/administrator/listBan.do" class="displaytag"
	pagesize="5">

	<display:column titleKey="chorbi.name" sortable="false">
		<a href="chorbi/administrator/profile.do?chorbiId=${row.id}"><jstl:out
				value="${row.name}"></jstl:out></a>
	</display:column>
	<display:column property="description" titleKey="chorbi.description" />
	<display:column property="relationship" titleKey="chorbi.looking" />
	<display:column titleKey="chorbi.genre">


		<jstl:if test="${row.genre=='man'}">
			<spring:message code="chorbi.man" />


		</jstl:if>
		<jstl:if test="${row.genre=='woman'}">
			<spring:message code="chorbi.woman" />


		</jstl:if>


	</display:column>

	<display:column>
		<jstl:if test="${row.banned==false}">
			<form:form method="POST" action="chorbi/administrator/listBan.do">
				<input type="hidden" name="chorbi" value="${row.id}" />

				<input type="submit" name="ban"
					value="<spring:message code="chorbi.ban" />" />
			</form:form>

		</jstl:if>
		<jstl:if test="${row.banned==true}">
			<form:form method="POST" action="chorbi/administrator/listBan.do">
				<input type="hidden" name="chorbi" value="${row.id}" />

				<input type="submit" name="unban"
					value="<spring:message code="chorbi.unban" />" />
			</form:form>

		</jstl:if>
	</display:column>
</display:table>
