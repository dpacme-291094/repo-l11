<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>



<display:table name="chorbies" id="row" requestURI="${uri}" pagesize="5"
	class="displaytag">


	<jstl:if test="${isLiker==true }">
		<display:column titleKey="chorbi.name" sortable="false">
			<jstl:if test="${isAdmin==true }">
				<a
					href="chorbi/administrator/profile.do?chorbiId=${row.likesSender.id}"><jstl:out
						value="${row.likesSender.name}"></jstl:out></a>
			</jstl:if>
			<jstl:if test="${isAdmin==false }">
				<a href="chorbies/chorbi/profile.do?chorbiId=${row.likesSender.id}"><jstl:out
						value="${row.likesSender.name}"></jstl:out></a>
			</jstl:if>
		</display:column>
		<display:column property="likesSender.surname"
			titleKey="chorbi.surname" sortable="false" />
		<display:column property="likesSender.description"
			titleKey="chorbi.description" />
		<display:column property="likesSender.relationship"
			titleKey="chorbi.looking" />
		<display:column titleKey="chorbi.genre">


			<jstl:if test="${row.likesSender.genre=='man'}">
				<spring:message code="chorbi.man" />


			</jstl:if>
			<jstl:if test="${row.likesSender.genre=='woman'}">
				<spring:message code="chorbi.woman" />


			</jstl:if>
		</display:column>

		<display:column property="comment" titleKey="chorbi.comment">


		</display:column>
	</jstl:if>


	<jstl:if test="${isLiker==null }">
		<display:column titleKey="chorbi.name" sortable="false">
			<jstl:if test="${isAdmin==true }">
				<a href="chorbi/administrator/profile.do?chorbiId=${row.id}"><jstl:out
						value="${row.name}"></jstl:out></a>
			</jstl:if>
			<jstl:if test="${isAdmin==false }">
				<a href="chorbies/chorbi/profile.do?chorbiId=${row.id}"><jstl:out
						value="${row.name}"></jstl:out></a>
			</jstl:if>
		</display:column>
		<display:column property="surname" titleKey="chorbi.surname"
			sortable="false" />
		<display:column property="description" titleKey="chorbi.description" />
		<display:column property="relationship" titleKey="chorbi.looking" />
		<display:column titleKey="chorbi.genre">


			<jstl:if test="${row.genre=='man'}">
				<spring:message code="chorbi.man" />


			</jstl:if>
			<jstl:if test="${row.genre=='woman'}">
				<spring:message code="chorbi.woman" />


			</jstl:if>



		</display:column>
	</jstl:if>




</display:table>

