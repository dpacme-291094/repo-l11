<%--
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="chorbi/register.do" modelAttribute="formChorbi">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount.Authorities" />
	<form:hidden path="chirpsReceived" />
	<form:hidden path="chirpsSent" />
	<form:hidden path="likesReceived" />
	<form:hidden path="likesSent" />
	<form:hidden path="banned" />
	<form:hidden path="search" />
	<form:hidden path="totalFee" />
	<form:hidden path="lastPayment" />
	<form:hidden path="events" />
	<table id="formus">
		<tr>
			<td><u>
					<h3>
						<spring:message code="chorbi.personal" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="chorbi.name" path="name" />
		<acme:textbox code="chorbi.surname" path="surname" />
		<acme:textbox code="chorbi.email" path="email" />
		<acme:textbox code="chorbi.phone" path="phone" />
		<acme:textbox code="chorbi.picture" path="picture" />
		<acme:textbox code="chorbi.birthDate" path="birthDate" />
		<acme:textbox code="chorbi.description" path="description" />
		<acme:selectModified code="chorbi.genre" path="genre" items="${genre}"
			id="genre" />
		<acme:selectModified code="chorbi.relationship" path="relationship"
			items="${relationship}" id="relationship" />

		<tr>
			<td><u>
					<h3>
						<spring:message code="chorbi.coordinates" />
					</h3>
			</u></td>
		</tr>

		<acme:textbox code="chorbi.country" path="coordinates.country" />
		<acme:textbox code="chorbi.state" path="coordinates.state" />
		<acme:textbox code="chorbi.province" path="coordinates.province" />
		<acme:textbox code="chorbi.city" path="coordinates.city" />

		<tr>
			<td><u>
					<h3>
						<spring:message code="chorbi.userac" />
					</h3>
			</u></td>
		</tr>
		<acme:textbox code="chorbi.userAccount" path="userAccount.username" />

		<acme:password code="chorbi.password" path="userAccount.password" />
		<tr>
			<td><b><spring:message code="chorbi.repassword" /></b></td>
			<td><input type="password" name="repassword" /></td>
		</tr>


	</table>

	<input type="checkbox" name="terms">

	<spring:message code="chorbi.terms" />
	<br />
	<br />

	<acme:submit name="save" code="chorbi.register" />
	<acme:cancel url="welcome/index.do" code="chorbi.cancel" />
</form:form>



