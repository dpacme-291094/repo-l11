<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<display:table name="chorbies" id="row"
	requestURI="chorbi/administrator/fee.do" class="displaytag"
	pagesize="5">

	<display:column titleKey="chorbi.name" sortable="false">

		<a href="chorbi/administrator/profile.do?chorbiId=${row.id}"> <jstl:out
				value="${row.name}"></jstl:out>
		</a>

	</display:column>
	<display:column property="surname" titleKey="chorbi.surname"
		sortable="false" />
	<display:column property="lastPayment" titleKey="chorbi.lastPayment" />
	<display:column property="totalFee" titleKey="chorbi.fee" />
	<display:column>
		<jstl:if test="${row.totalFee >0 }">
			<form:form method="POST" action="chorbi/administrator/fee.do">
				<input type="hidden" name="chorbi" value="${row.id}" />

				<input type="submit" name="pay"
					value="<spring:message code="chorbi.pay" />"
					onclick="return confirm('<spring:message code="confirm.pay"/> ${row.totalFee} Euros?')" />
			</form:form>
		</jstl:if>
	</display:column>

</display:table>
