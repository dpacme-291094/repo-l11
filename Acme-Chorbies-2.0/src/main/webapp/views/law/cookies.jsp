<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<p><spring:message code="law.cookies.context1"/></p>
<h2><spring:message code="law.cookies.title2"/></h2>
<p><spring:message code="law.cookies.context2"/></p>
<p><spring:message code="law.cookies.context2.1"/></p>
<p><spring:message code="law.cookies.context2.2"/></p>
<p><spring:message code="law.cookies.context2.3"/></p>
<p><spring:message code="law.cookies.context2.4"/></p>
<h2><spring:message code="law.cookies.title3"/></h2>
<p><spring:message code="law.cookies.context3"/></p>
<p><b><spring:message code="law.cookies.context3.1"/></b></p>
<p><spring:message code="law.cookies.context3.2"/></p>
<a href="https://support.google.com/chrome/answer/95647?hl=es">Chrome</a><br/>
<a href="https://support.microsoft.com/en-us/products/windows?os=windows-10">Explorer</a><br/>
<a href="https://support.mozilla.org/t5/How-To/Habilitar-y-deshabilitar-cookies-que-los-sitios-web-utilizan/ta-p/34665">Firefox</a><br/>
<a href="https://support.apple.com/kb/ph5042?locale=en_US">Safari</a><br/>
<p><spring:message code="law.cookies.context3.3"/></p>
