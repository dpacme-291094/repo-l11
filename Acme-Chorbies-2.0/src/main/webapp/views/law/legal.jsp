<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<h2><spring:message code="law.legal.title1"/></h2>
<p><spring:message code="law.legal.context1"/></p>

<h2><spring:message code="law.legal.title2"/></h2>
<p><spring:message code="law.legal.context2"/></p>

<h2><spring:message code="law.legal.title3"/></h2>
<p><spring:message code="law.legal.context3"/></p>

<h2><spring:message code="law.legal.title4"/></h2>
<p><spring:message code="law.legal.context4"/></p>

<h2><spring:message code="law.legal.title5"/></h2>
<p><spring:message code="law.legal.context5"/></p>

<h2><spring:message code="law.legal.title6"/></h2>
<p><spring:message code="law.legal.context6"/></p>

<h2><spring:message code="law.legal.title7"/></h2>
<p><spring:message code="law.legal.context7"/></p>

<h2><spring:message code="law.legal.title8"/></h2>
<p><spring:message code="law.legal.context8"/></p>

<h2><spring:message code="law.legal.title9"/></h2>
<p><spring:message code="law.legal.context9"/></p>

<h2><spring:message code="law.legal.title10"/></h2>
<p><spring:message code="law.legal.context10"/></p>

<h2><spring:message code="law.legal.title11"/></h2>
<p><spring:message code="law.legal.context11"/></p>

<h2><spring:message code="law.legal.title12"/></h2>
<p><spring:message code="law.legal.context12"/></p>


