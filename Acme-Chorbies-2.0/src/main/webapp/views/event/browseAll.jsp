<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date"%>
<%@ page import="domain.Event"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<display:table name="events" id="row" requestURI="event/browseAll.do"
	class="displaytag" pagesize="5">

	<jstl:choose>
		<jstl:when test="${row.moment<currentDate}">



			<display:column titleKey="event.photo" sortable="false"
				class="greyout">
				<img src="${row.photo}" height="200" width="150">
			</display:column>
			<display:column titleKey="event.title" sortable="false"
				class="greyout">


				<a href="event/profile.do?eventId=${row.id}"><jstl:out
						value="${row.title}"></jstl:out></a>

			</display:column>
			<display:column property="moment" titleKey="event.moment"
				sortable="false" class="greyout" />
			<display:column property="description" titleKey="event.description"
				sortable="false" class="greyout" />
			<display:column property="seats" titleKey="event.seats"
				sortable="true" class="greyout" />
		</jstl:when>

		<jstl:when
			test="${row.moment > currentDate and row.moment < currentDatePlusOneMonth and row.seats > 0 }">



			<display:column titleKey="event.photo" sortable="false"
				class="highlight">
				<img src="${row.photo}" height="200" width="150">
			</display:column>
			<display:column titleKey="event.title" sortable="false"
				class="highlight">

				<a href="event/profile.do?eventId=${row.id}"><jstl:out
						value="${row.title}"></jstl:out></a>
			</display:column>
			<display:column property="moment" titleKey="event.moment"
				sortable="false" class="highlight" />
			<display:column property="description" titleKey="event.description"
				sortable="false" class="highlight" />
			<display:column property="seats" titleKey="event.seats"
				sortable="true" class="highlight" />
		</jstl:when>


		<jstl:otherwise>


			<display:column titleKey="event.photo" sortable="false">
				<img src="${row.photo}" height="200" width="150">
			</display:column>
			<display:column titleKey="event.title" sortable="false">

				<a href="event/profile.do?eventId=${row.id}"><jstl:out
						value="${row.title}"></jstl:out></a>
			</display:column>
			<display:column property="moment" titleKey="event.moment"
				sortable="false" />
			<display:column property="description" titleKey="event.description"
				sortable="false" />
			<display:column property="seats" titleKey="event.seats"
				sortable="true" />
		</jstl:otherwise>
	</jstl:choose>

</display:table>
