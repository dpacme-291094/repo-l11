<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<table id="formus">


	<tr>
		<td><img src="${event.photo}" height="200" width="150"></td>
	</tr>
	<tr>
		<td></td>
	</tr>

	<tr>
		<td><b><spring:message code="event.title" /></b> <br /> <jstl:out
				value="${event.title}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="event.moment" /></b> <br /> <jstl:out
				value="${event.moment}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="event.description" /></b> <br /> <jstl:out
				value="${event.description}" /></td>
	</tr>
	<tr>
		<td><b><spring:message code="event.seats" /></b> <br /> <jstl:out
				value="${event.seats}" /></td>
	</tr>

</table>
<jstl:if
	test="${ischorbi == true && event.moment>currentDate && event.seats>0}">
	<jstl:if test="${isnotregistered == true}">

		<form:form method="POST" action="event/chorbi/profile.do">
			<input type="hidden" name="eventId" value="${event.id}" />

			<input type="submit" name="register"
				value="<spring:message code="event.register" />" />
		</form:form>
	</jstl:if>
	<jstl:if test="${isnotregistered == false}">


		<form:form method="POST" action="event/chorbi/profile.do">
			<input type="hidden" name="eventId" value="${event.id}" />

			<input type="submit" name="unregister"
				value="<spring:message code="event.unregister" />" />
		</form:form>
	</jstl:if>

</jstl:if>


