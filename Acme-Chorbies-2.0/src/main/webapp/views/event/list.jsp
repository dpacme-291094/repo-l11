<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<display:table name="events" id="row" requestURI="event/${uri}list.do"
	pagesize="5" class="displaytag">


	<display:column titleKey="event.photo" sortable="false">
		<img src="${row.photo}" width="100" height="120" />
	</display:column>
	<display:column titleKey="event.title" sortable="false">
		<a href="event/profile.do?eventId=${row.id}"><jstl:out
				value="${row.title}"></jstl:out></a>
	</display:column>

	<display:column property="moment" titleKey="event.moment"
		sortable="false" />
	<display:column property="description" titleKey="event.description"
		sortable="false" />
	<display:column sortable="true" titleKey="event.seats" property="seats" />
	<security:authorize access="hasRole('MANAGER')">
		<display:column>
			<form:form method="POST" action="event/manager/delete.do">
				<input type="hidden" name="eventId" value="${row.id}" />
				<input type="submit" name="delete"
					value="<spring:message code="event.delete" />"
					onclick="return confirm('<spring:message code="event.confirm.delete" />')" />&nbsp;
		</form:form>
		</display:column>

		<display:column>
			<form:form method="POST" action="event/manager/modify.do">
				<input type="hidden" name="eventId" value="${row.id}" />
				<input type="submit" name="modify"
					value="<spring:message code="event.modify" />" />&nbsp;
		</form:form>
		</display:column>

		<display:column>
			<jstl:if test="${fn:length(row.chorbies)>0}">
				<form:form method="POST" action="event/manager/send.do">
					<input type="hidden" name="eventId" value="${row.id}" />
					<input type="submit" name="send"
						value="<spring:message code="event.message" />" />
				</form:form>
			</jstl:if>
		</display:column>
	</security:authorize>
</display:table>
<security:authorize access="hasRole('MANAGER')">

	<a href="event/manager/create.do"> <spring:message
			code="event.create" />
	</a>
</security:authorize>
