<%--
 * list.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="chirps" id="row" requestURI="${ruta}"
	class="displaytag" pagesize="5">
	<display:column>
		<a href="chirp/actor/read.do?id=${row.id}&l=${whereIAm}"><spring:message
				code="chirp.read" /></a>
	</display:column>
	<display:column titleKey="chirp.sender">
		<jstl:out value="${row.userSender}" />
	</display:column>

	<display:column titleKey="chirp.recipient">
		<jstl:out value="${row.userRecipient}" />
	</display:column>

	<display:column property="moment" titleKey="chirp.moment"
		sortable="false" />
	<display:column property="subject" titleKey="chirp.subject"
		sortable="false" />

	<jstl:if test="${whereIAm!=null && whereIAm=='i'}">
		<display:column>
			<form:form method="POST" action="chirp/actor/send.do">
				<input type="hidden" name="id" value="${row.id}" />
				<input type="hidden" name="whereIAm" value="${whereIAm}" />
				<acme:submit name="reply" code="chirp.reply" />
			</form:form>
		</display:column>
	</jstl:if>

	<jstl:if test="${whereIAm!=null}">
		<display:column>
			<form:form method="POST" action="chirp/actor/send.do">
				<input type="hidden" name="id" value="${row.id}" />
				<input type="hidden" name="whereIAm" value="${whereIAm}" />
				<acme:submit name="forward" code="chirp.forward" />
			</form:form>
		</display:column>
	</jstl:if>

	<jstl:if test="${whereIAm!=null}">
		<display:column>
			<form:form method="POST" action="chirp/actor/delete.do">
				<input type="hidden" name="id" value="${row.id}" />
				<input type="hidden" name="whereIAm" value="${whereIAm}" />
				<input type="submit" name="delete"
					value="<spring:message code="chirp.delete" />"
					onclick="return confirm('<spring:message code="chirp.confirm.delete" />')" />&nbsp;
	</form:form>
		</display:column>
	</jstl:if>
</display:table>

<div>
	<security:authorize access="hasRole('CHORBI')">
		<a href="chorbies/chorbi/list.do"> <spring:message
				code="chirp.create" />
		</a>
	</security:authorize>
</div>
