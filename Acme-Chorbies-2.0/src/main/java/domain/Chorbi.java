
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = {
	@Index(columnList = "description, relationship, banned, birthDate, genre, city, country, state, province")
})
public class Chorbi extends Actor {

	// Constructors -----------------------------------------------------------

	public Chorbi() {
		super();
	}


	// Attributes -------------------------------------------------------------

	private String		picture;
	private String		description;
	private String		relationship;
	private Date		birthDate;
	private String		genre;
	private Coordinates	coordinates;
	private CreditCard	creditCard;
	private boolean		banned;
	private double		totalFee;
	private Date		lastPayment;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@URL
	public String getPicture() {
		return this.picture;
	}

	public void setPicture(final String picture) {
		this.picture = picture;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotBlank
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}
	@NotBlank
	@Pattern(regexp = "^(activities|friendship|love)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getRelationship() {
		return this.relationship;
	}

	public void setRelationship(final String relationship) {
		this.relationship = relationship;
	}
	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(final Date birthDate) {
		this.birthDate = birthDate;
	}
	@NotBlank
	@Pattern(regexp = "^(man|woman)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getGenre() {
		return this.genre;
	}

	public void setGenre(final String genre) {
		this.genre = genre;
	}
	@Valid
	@NotNull
	public Coordinates getCoordinates() {
		return this.coordinates;
	}

	public void setCoordinates(final Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	public CreditCard getCreditCard() {
		return this.creditCard;
	}

	public void setCreditCard(final CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public boolean isBanned() {
		return this.banned;
	}

	public void setBanned(final boolean banned) {
		this.banned = banned;
	}

	@Min(0)
	public double getTotalFee() {
		return this.totalFee;
	}

	public void setTotalFee(final double totalFee) {
		this.totalFee = totalFee;
	}

	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getLastPayment() {
		return this.lastPayment;
	}

	public void setLastPayment(final Date lastPayment) {
		this.lastPayment = lastPayment;
	}


	//Relationships ----------------------------------------------------
	private Collection<Likes>	likesReceived;
	private Collection<Likes>	likesSent;
	private SearchTemplate		search;
	private Collection<Event>	events;


	@NotNull
	@Valid
	@ManyToMany
	public Collection<Event> getEvents() {
		return this.events;
	}

	public void setEvents(final Collection<Event> events) {
		this.events = events;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "likesRecipient")
	public Collection<Likes> getLikesReceived() {
		return this.likesReceived;
	}

	public void setLikesReceived(final Collection<Likes> likesReceived) {
		this.likesReceived = likesReceived;
	}
	@Valid
	@NotNull
	@OneToMany(mappedBy = "likesSender")
	public Collection<Likes> getLikesSent() {
		return this.likesSent;
	}

	public void setLikesSent(final Collection<Likes> likesSent) {
		this.likesSent = likesSent;
	}

	@OneToOne(mappedBy = "chorbi")
	public SearchTemplate getSearch() {
		return this.search;
	}

	public void setSearch(final SearchTemplate search) {
		this.search = search;
	}
}
