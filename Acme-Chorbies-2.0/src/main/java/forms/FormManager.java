
package forms;

import java.util.Collection;

import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

import security.UserAccount;
import domain.Chirp;
import domain.CreditCard;
import domain.Event;

public class FormManager {

	private int					id;
	private int					version;
	private UserAccount			userAccount;
	private String				name;
	private String				surname;
	private String				phone;
	private String				email;
	private double				totalFee;
	private Collection<Event>	events;
	private String				VATNumber;
	private String				company;
	private CreditCard			creditCard;
	private Collection<Chirp>	chirpsReceived;
	private Collection<Chirp>	chirpsSent;


	@SafeHtml(whitelistType = WhiteListType.NONE)
	public int getId() {
		return this.id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	public void setUserAccount(final UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSurname() {
		return this.surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	@Email
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@NotBlank
	@Pattern(regexp = "^(([+])([0-9]{1,3})([ ])?)?(([0-9]{3}([ ])?[0-9]{3}([ ])?[0-9]{3})|([0-9]{3}([ ])?[0-9]{2}([ ])?[0-9]{2}([ ])?[0-9]{2}))$$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	@Min(0)
	public double getTotalFee() {
		return this.totalFee;
	}

	public void setTotalFee(final double totalFee) {
		this.totalFee = totalFee;
	}

	@Valid
	@NotNull
	public CreditCard getCreditCard() {
		return this.creditCard;
	}

	public void setCreditCard(final CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	@NotNull
	@Valid
	@OneToMany(mappedBy = "manager")
	public Collection<Event> getEvents() {
		return this.events;
	}

	public void setEvents(final Collection<Event> events) {
		this.events = events;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getVATNumber() {
		return this.VATNumber;
	}

	public void setVATNumber(final String vATNumber) {
		this.VATNumber = vATNumber;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getCompany() {
		return this.company;
	}

	public void setCompany(final String company) {
		this.company = company;
	}

	@Valid
	@NotNull
	@OneToMany(mappedBy = "chirpRecipient")
	public Collection<Chirp> getChirpsReceived() {
		return this.chirpsReceived;
	}

	public void setChirpsReceived(final Collection<Chirp> chirpsReceived) {
		this.chirpsReceived = chirpsReceived;
	}
	@Valid
	@NotNull
	@OneToMany(mappedBy = "chirpSender")
	public Collection<Chirp> getChirpsSent() {
		return this.chirpsSent;
	}

	public void setChirpsSent(final Collection<Chirp> chirpsSent) {
		this.chirpsSent = chirpsSent;
	}
}
