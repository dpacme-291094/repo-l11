
package forms;

import java.util.Date;

import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

import domain.Chirp;

public class FormChirp {

	// Attributes -------------------------------------------------------------
	private Date	moment;
	private String	subject;
	private String	text;
	private String	attachments;
	private String	userSender;
	private String	userRecipient;


	public FormChirp() {
		super();
	}

	public FormChirp(final Chirp m) {
		this.setUserRecipient(m.getUserRecipient());
		this.setUserSender(m.getUserSender());
		this.setText(m.getText());
		this.setSubject(m.getSubject());
		this.setAttachments(m.getAttachments());
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 60000);
		this.setMoment(currentDate);

	}

	@NotBlank
	public String getUserSender() {
		return this.userSender;
	}

	public void setUserSender(final String userSender) {
		this.userSender = userSender;
	}

	@NotBlank
	public String getUserRecipient() {
		return this.userRecipient;
	}

	public void setUserRecipient(final String userRecipient) {
		this.userRecipient = userRecipient;
	}

	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getText() {
		return this.text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public String getAttachments() {
		return this.attachments;
	}

	public void setAttachments(final String attachments) {
		this.attachments = attachments;
	}

}
