
package controllers;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ChorbiService;
import services.EventService;
import domain.Actor;
import domain.Chorbi;
import domain.Event;

@Controller
@RequestMapping("/event")
public class EventController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private EventService	eventService;

	@Autowired
	private ActorService	actorService;
	@Autowired
	private ChorbiService	chorbiService;


	// Constructor ---------------------------------------------------------------
	public EventController() {
		super();
	}

	// List -------------------------------------
	@RequestMapping(value = "/browseOneMonth", method = RequestMethod.GET)
	public ModelAndView browseOneMonth() {
		final ModelAndView result;

		final Collection<Event> events = this.eventService.findLessOneMonth();
		result = new ModelAndView("event/browseOneMonth");
		result.addObject("events", events);
		return result;

	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profile(@Valid final Integer eventId) {
		if (eventId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		ModelAndView result;
		final Date currentDate = new Date();
		final Event event = this.eventService.findOne(eventId);

		if (event == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");
		else {
			result = new ModelAndView("event/profile");
			result.addObject("event", event);
			if (!LoginService.isAnonymous()) {
				final Actor a = this.actorService.findByPrincipal();
				if (a instanceof Chorbi)
					result.addObject("ischorbi", true);
				if (event.getChorbies().contains(a))
					result.addObject("isnotregistered", false);
				else
					result.addObject("isnotregistered", true);
			} else
				result.addObject("ischorbi", false);
			result.addObject("currentDate", currentDate);
		}
		return result;
	}

	@RequestMapping(value = "/browseAll", method = RequestMethod.GET)
	public ModelAndView browseAll() {
		final ModelAndView result;
		final Date currentDate = new Date();
		final Calendar currentCalPlusOneMonth = Calendar.getInstance();
		currentCalPlusOneMonth.add(Calendar.DATE, 30);
		final Date currentDatePlusOneMonth = currentCalPlusOneMonth.getTime();

		final Collection<Event> events = this.eventService.findAll();

		result = new ModelAndView("event/browseAll");
		result.addObject("events", events);
		result.addObject("currentDate", currentDate);
		result.addObject("currentDatePlusOneMonth", currentDatePlusOneMonth);
		return result;

	}
}
