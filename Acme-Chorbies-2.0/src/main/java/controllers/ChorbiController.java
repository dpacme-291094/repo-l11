
package controllers;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ChorbiService;
import domain.Chorbi;
import forms.FormChorbi;

@Controller
@RequestMapping("/chorbi")
public class ChorbiController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private ChorbiService	chorbiService;
	@Autowired
	private ActorService	actorService;


	// Constructor ---------------------------------------------------------------
	public ChorbiController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView create() {
		final Collection<String> genre = Arrays.asList("man", "woman");
		final Collection<String> relationship = Arrays.asList("activities", "friendship", "love");

		final FormChorbi formChorbi = this.chorbiService.createForm();
		final ModelAndView result = new ModelAndView("chorbi/register");
		result.addObject("formChorbi", formChorbi);
		result.addObject("genre", genre);
		result.addObject("relationship", relationship);
		return result;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@ModelAttribute("formChorbi") final FormChorbi formChorbi, final BindingResult binding, final boolean terms, final String repassword) {
		ModelAndView result;
		final String redirectView;

		Chorbi chorbi = this.chorbiService.reconstruct(formChorbi, binding);
		if (binding.hasErrors())
			result = this.createEditModelAndView(formChorbi);
		else
			try {
				final String username = chorbi.getUserAccount().getUsername();
				if (this.actorService.existsUsername(username))
					result = this.createEditModelAndView(formChorbi, "actor.commit.error");

				else if (!terms)
					result = this.createEditModelAndView(formChorbi, "actor.error.terms");
				else if (!repassword.equals(chorbi.getUserAccount().getPassword()))
					result = this.createEditModelAndView(formChorbi, "actor.pass");
				else if (!this.filterAge(chorbi.getBirthDate()))
					result = this.createEditModelAndView(formChorbi, "chorbi.error.invalid.age");
				else {
					chorbi = this.comprobarChorbi(chorbi);

					this.chorbiService.save(chorbi);
					redirectView = "redirect:../security/login.do";
					result = new ModelAndView(redirectView);
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(formChorbi, "actor.commit.error2");
			}

		return result;
	}
	// Other Ancillary Methods -------------------------------------
	protected ModelAndView createEditModelAndView(final FormChorbi chorbi) {
		ModelAndView result;
		result = this.createEditModelAndView(chorbi, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final FormChorbi chorbi, final String message) {
		ModelAndView result;
		final Collection<String> genre = Arrays.asList("man", "woman");
		final Collection<String> relationship = Arrays.asList("activities", "friendship", "love");

		result = new ModelAndView("chorbi/register");
		result.addObject("chorbi", chorbi);
		result.addObject("message", message);

		result.addObject("genre", genre);
		result.addObject("relationship", relationship);
		return result;
	}

	private boolean filterAge(final Date birthDate) {
		boolean valid = false;
		final int limitAge = 18;
		final Date currentDate = new Date();
		final int yearsDifference = currentDate.getYear() - birthDate.getYear();

		if (yearsDifference > limitAge)
			valid = true;
		else if (yearsDifference == limitAge)
			if (currentDate.getMonth() > birthDate.getMonth())
				valid = true;
			else if (currentDate.getMonth() == birthDate.getMonth() && currentDate.getDate() >= birthDate.getDate())
				valid = true;
		return valid;
	}

	private Chorbi comprobarChorbi(final Chorbi chorbi) {
		if (chorbi.getCoordinates().getCountry() == null)
			chorbi.getCoordinates().setCountry("");
		if (chorbi.getCoordinates().getState() == null)
			chorbi.getCoordinates().setState("");
		if (chorbi.getCoordinates().getProvince() == null)
			chorbi.getCoordinates().setProvince("");
		if (chorbi.getDescription() == null)
			chorbi.setDescription("");

		return chorbi;
	}
}
