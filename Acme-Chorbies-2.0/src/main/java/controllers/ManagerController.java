
package controllers;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ManagerService;
import domain.Manager;
import forms.FormManager;

@Controller
@RequestMapping("/manager")
public class ManagerController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private ManagerService	managerService;
	@Autowired
	private ActorService	actorService;


	// Constructor ---------------------------------------------------------------
	public ManagerController() {
		super();
	}

	// Create -------------------------------------
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView create() {
		final FormManager formManager = this.managerService.createForm();
		final ModelAndView result = new ModelAndView("manager/register");
		result.addObject("formManager", formManager);
		final Collection<String> brandNames = Arrays.asList("VISA", "MASTERCARD", "DISCOVER", "DINNERS", "AMEX");
		result.addObject("brandNames", brandNames);
		return result;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@ModelAttribute("formManager") final FormManager formManager, final BindingResult binding, final boolean terms, final String repassword) {
		ModelAndView result;
		final String redirectView;

		final Manager manager = this.managerService.reconstruct(formManager, binding);
		if (binding.hasErrors())
			result = this.createEditModelAndView(formManager);
		else
			try {
				final String username = manager.getUserAccount().getUsername();
				if (!this.fechaValida(manager.getCreditCard().getExpirationMonth(), manager.getCreditCard().getExpirationYear()))
					result = this.createEditModelAndView(formManager, "actor.commit.error4");

				else if (this.actorService.existsUsername(username))
					result = this.createEditModelAndView(formManager, "actor.commit.error");

				else if (!terms)
					result = this.createEditModelAndView(formManager, "actor.error.terms");
				else if (!repassword.equals(manager.getUserAccount().getPassword()))
					result = this.createEditModelAndView(formManager, "actor.pass");
				else {

					this.managerService.save(manager);
					redirectView = "redirect:../security/login.do";
					result = new ModelAndView(redirectView);
				}
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(formManager, "actor.commit.error2");
			}

		return result;
	}
	// Other Ancillary Methods -------------------------------------
	protected ModelAndView createEditModelAndView(final FormManager manager) {
		ModelAndView result;
		result = this.createEditModelAndView(manager, null);
		return result;
	}

	private ModelAndView createEditModelAndView(final FormManager manager, final String message) {
		ModelAndView result;

		result = new ModelAndView("manager/register");
		result.addObject("manager", manager);
		result.addObject("message", message);
		final Collection<String> brandNames = Arrays.asList("VISA", "MASTERCARD", "DISCOVER", "DINNERS", "AMEX");
		result.addObject("brandNames", brandNames);
		return result;
	}

	@SuppressWarnings("deprecation")
	private boolean fechaValida(final int mes, final int ano) {
		boolean res = false;
		final Date d = new Date();
		d.setSeconds(7 * 86400);
		if (d.getYear() + 1900 < ano)
			res = true;
		else if (d.getYear() + 1900 == ano && d.getMonth() < mes - 1)
			res = true;

		return res;
	}
}
