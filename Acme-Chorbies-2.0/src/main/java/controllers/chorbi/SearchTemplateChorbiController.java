
package controllers.chorbi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ChorbiService;
import services.ConfigurationService;
import services.SearchTemplateService;
import controllers.AbstractController;
import domain.Chorbi;
import domain.SearchTemplate;

@Controller
@RequestMapping("/searchTemplate/chorbi")
public class SearchTemplateChorbiController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private SearchTemplateService	searchTemplateService;

	@Autowired
	private ChorbiService			chorbiService;

	@Autowired
	private ConfigurationService	configurationService;


	// Constructors -----------------------------------------------------------

	public SearchTemplateChorbiController() {
		super();
	}

	// Listing ----------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		final Chorbi a = this.chorbiService.findByPrincipal();

		if (a.getCreditCard() == null || !this.fechaValida(a.getCreditCard().getExpirationMonth(), a.getCreditCard().getExpirationYear())) {

			final String banner = this.bannerAleatorio(this.getActiveBanners(this.configurationService.findAll().getBanners()));
			result = new ModelAndView("welcome/index");
			result.addObject("message", "searchTemplate.cc.error");
			result.addObject("banner", banner);
		} else {

			if (this.comprobarCache(a.getSearch())) {

				final Collection<Chorbi> cc = new HashSet<Chorbi>();
				a.getSearch().setChorbies(cc);
				this.searchTemplateService.save(a.getSearch());

			}
			final Collection<String> genre = Arrays.asList("man", "woman", "both");
			final Collection<String> relationship = Arrays.asList("all", "activities", "friendship", "love");
			result = new ModelAndView("searchTemplate/list");
			result.addObject("genre", genre);
			result.addObject("relationship", relationship);
			result.addObject("searchTemplate", a.getSearch());
			result.addObject("hayChorbies", this.hayChorbies(a.getSearch()));
			result.addObject("chorbies", a.getSearch().getChorbies());
		}

		return result;
	}
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final SearchTemplate st, final BindingResult binding) {

		ModelAndView result;
		final SearchTemplate st1 = this.searchTemplateService.reconstruct(st, binding);

		if (binding.hasErrors())
			result = this.createEditModelAndView(st1);
		else
			try {

				final List<String> values = this.serachValidValues(st1);
				final Collection<Chorbi> cc;
				if (st.getAge() == null)
					cc = this.chorbiService.findChorbiesBySearch(values.get(3), values.get(0), values.get(1), values.get(2), values.get(6), values.get(5), values.get(4));
				else {
					final Collection<Chorbi> cc1 = this.chorbiService.findChorbiesBySearch(values.get(3), values.get(0), values.get(1), values.get(2), values.get(6), values.get(5), values.get(4));
					cc = this.inAgeChorbies(cc1, st1.getAge());

				}
				st1.setChorbies(cc);

				final Date currentDate = new Date();
				currentDate.setTime(currentDate.getTime() - 6000);

				st1.setMoment(currentDate);
				final SearchTemplate res = this.searchTemplateService.save(st1);

				result = new ModelAndView("searchTemplate/list");
				final Collection<String> genre = Arrays.asList("man", "woman", "both");
				final Collection<String> relationship = Arrays.asList("all", "activities", "friendship", "love");
				result.addObject("genre", genre);
				result.addObject("relationship", relationship);
				result.addObject("searchTemplate", res);
				result.addObject("hayChorbies", this.hayChorbies(res));
				result.addObject("chorbies", st1.getChorbies());

			} catch (final Throwable oops) {
				result = this.createEditModelAndView2(st1, "searchTemplate.commit.error");
			}
		return result;
	}
	@SuppressWarnings("deprecation")
	private Collection<Chorbi> inAgeChorbies(final Collection<Chorbi> cc1, final Integer age) {
		final Collection<Chorbi> result = new HashSet<Chorbi>();
		final Date first = new Date();
		first.setSeconds(-((age + 5) * 365 * 24 * 60 * 60));
		final Date second = new Date();
		second.setSeconds(-((age - 5) * 365 * 24 * 60 * 60));

		for (final Chorbi a : cc1)
			if (a.getBirthDate().after(first) && a.getBirthDate().before(second))
				result.add(a);

		return result;
	}
	protected ModelAndView createEditModelAndView(final SearchTemplate st) {

		ModelAndView result;

		result = this.createEditModelAndView2(st, null);

		return result;
	}

	protected ModelAndView createEditModelAndView2(final SearchTemplate st, final String message) {
		ModelAndView result;

		result = new ModelAndView("searchTemplate/list");
		result.addObject("searchTemplate", st);
		result.addObject("message", message);
		return result;
	}

	private int hayChorbies(final SearchTemplate st) {
		int res = 0;

		if (st.getChorbies().size() > 0)
			res = 1;
		return res;
	}

	private List<String> serachValidValues(final SearchTemplate st) {
		final List<String> res = new ArrayList<String>();

		if (st.getCoordinates() == null) {
			res.add(0, "%");
			res.add(1, "%");
			res.add(2, "%");
			res.add(3, "%");
		} else {
			if (st.getCoordinates().getCountry() == null || st.getCoordinates().getCountry().isEmpty())
				res.add(0, "%");
			else
				res.add(0, st.getCoordinates().getCountry());

			if (st.getCoordinates().getState() == null || st.getCoordinates().getState().isEmpty())
				res.add(1, "%");
			else
				res.add(1, st.getCoordinates().getState());

			if (st.getCoordinates().getProvince() == null || st.getCoordinates().getProvince().isEmpty())
				res.add(2, "%");
			else
				res.add(2, st.getCoordinates().getProvince());

			if (st.getCoordinates().getCity() == null || st.getCoordinates().getCity().isEmpty())
				res.add(3, "%");
			else
				res.add(3, st.getCoordinates().getCity());
		}
		if (st.getRelationship().equals("all"))
			res.add(4, "%");
		else
			res.add(4, st.getRelationship());

		if (st.getKeyword() == null || st.getKeyword().isEmpty())
			res.add(5, "");
		else
			res.add(5, st.getKeyword());

		if (st.getGenre().equals("both"))
			res.add(6, "%");
		else
			res.add(6, st.getGenre());

		return res;
	}
	@SuppressWarnings("deprecation")
	private boolean comprobarCache(final SearchTemplate st) {
		boolean res = false;
		final String[] conf = this.configurationService.findAll().getSearchHours().split(":");
		final int tiempo = (Integer.parseInt(conf[0]) * 60 * 60) + (Integer.parseInt(conf[1]) * 60) + Integer.parseInt(conf[2]);

		final Date d = new Date();
		d.setSeconds(-tiempo);

		if (st.getMoment().before(d))
			res = true;
		else if (st.getMoment().after(d) || st.getMoment().equals(d))
			res = false;
		else
			res = false;

		return res;
	}
	@SuppressWarnings("deprecation")
	private boolean fechaValida(final int mes, final int ano) {
		boolean res = false;
		final Date d = new Date();
		d.setSeconds(7 * 86400);
		if (d.getYear() + 1900 < ano)
			res = true;
		else if (d.getYear() + 1900 == ano && d.getMonth() < mes - 1)
			res = true;

		return res;
	}

	private Collection<String> getActiveBanners(final String c) {
		final Collection<String> cs = new LinkedList<String>();

		cs.addAll(Arrays.asList(c.split(",")));

		return cs;
	}

	private String bannerAleatorio(final Collection<String> activeBanners) {
		String b = "";
		final Random rnd = new Random();
		final LinkedList<String> ls = new LinkedList<String>();
		ls.addAll(activeBanners);

		final int num = (int) (rnd.nextDouble() * ls.size() + 0);
		if (ls.size() > 0)
			b = ls.get(num);
		else
			b = "ERROR";

		return b;
	}
}
