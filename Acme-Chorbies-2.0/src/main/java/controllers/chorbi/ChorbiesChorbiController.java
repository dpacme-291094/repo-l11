
package controllers.chorbi;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ChorbiService;
import services.ConfigurationService;
import services.LikesService;
import controllers.AbstractController;
import domain.Actor;
import domain.Chorbi;
import domain.Likes;

@Controller
@RequestMapping("/chorbies/chorbi")
public class ChorbiesChorbiController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private ChorbiService			chorbiService;
	@Autowired
	private ActorService			actorService;
	@Autowired
	private LikesService			likeService;
	@Autowired
	private ConfigurationService	configurationService;


	// Constructor ---------------------------------------------------------------
	public ChorbiesChorbiController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		final Actor a = this.actorService.findByPrincipal();

		if (a instanceof Chorbi) {
			final Chorbi c = this.chorbiService.findByPrincipal();
			final Collection<Chorbi> chorbies = this.chorbiService.findAll();
			chorbies.remove(c);
			result = new ModelAndView("chorbi/list");
			result.addObject("chorbies", chorbies);
			result.addObject("uri", "chorbies/chorbi/list.do");
			result.addObject("isAdmin", false);

		} else {
			final Collection<Chorbi> chorbies = this.chorbiService.findAll();
			result = new ModelAndView("chorbi/list");
			result.addObject("chorbies", chorbies);
			result.addObject("uri", "chorbies/chorbi/list.do");
			result.addObject("isAdmin", true);
		}

		return result;

	}
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profile(@Valid final int chorbiId) {
		ModelAndView result;

		final Chorbi chorbi = this.chorbiService.findOne(chorbiId);

		if (chorbi == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");
		else {

			result = new ModelAndView("chorbi/profile");
			result.addObject("avgLikes", this.chorbiService.findStars(chorbi.getId()));
			result.addObject("chorbi", chorbi);
			result.addObject("isChorbi", true);

			if (chorbi.getId() == this.chorbiService.findByPrincipal().getId())
				result.addObject("myself", true);
			else
				result.addObject("myself", false);
			if (this.chorbiService.findByPrincipal().getLikesSent() != null && !this.chorbiService.findByPrincipal().getLikesSent().isEmpty())
				for (final Likes a : this.chorbiService.findByPrincipal().getLikesSent())
					if (a.getLikesRecipient().getId() == chorbi.getId()) {
						result.addObject("following", true);
						result.addObject("likeId", a.getId());
						break;
					} else
						result.addObject("following", false);
			else
				result.addObject("following", false);
			result.addObject("age", ChorbiService.getAge(chorbi.getBirthDate()));
		}
		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "like")
	public ModelAndView like(@Valid final Integer chorbiId) {

		ModelAndView result;
		final Chorbi cc = this.chorbiService.findOne(chorbiId);

		final Collection<Integer> rates = Arrays.asList(0, 1, 2, 3);
		result = new ModelAndView("like/edit");
		result.addObject("chorbi", cc);
		result.addObject("rates", rates);
		result.addObject("like", this.likeService.create(this.chorbiService.findOne(chorbiId)));
		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "dislike")
	public ModelAndView dislike(@Valid final Integer likeId) {

		final ModelAndView result;

		final Likes l = this.likeService.findOne(likeId);
		final Chorbi recipient = l.getLikesRecipient();
		this.likeService.dislike(l);
		result = new ModelAndView("redirect: ../../../../chorbies/chorbi/profile.do?chorbiId=" + recipient.getId());

		return result;
	}
	@RequestMapping(value = "/likesList", method = RequestMethod.GET)
	public ModelAndView likesList(@Valid final int chorbiId) {

		final Actor ac = this.actorService.findByPrincipal();
		final Chorbi c = this.chorbiService.findOne(chorbiId);
		ModelAndView result;
		if (ac instanceof Chorbi) {
			final Chorbi a = this.chorbiService.findByPrincipal();

			if (c == null)
				throw new RuntimeException("Oops! You should not be here. Go back to home page.");
			else if ((a.equals(c) && a.getCreditCard() == null) || (a.equals(c) && !this.fechaValida(a.getCreditCard().getExpirationMonth(), a.getCreditCard().getExpirationYear()))) {

				final String banner = this.bannerAleatorio(this.getActiveBanners(this.configurationService.findAll().getBanners()));
				result = new ModelAndView("welcome/index");
				result.addObject("message", "searchTemplate.cc.error2");
				result.addObject("banner", banner);
			} else {

				final Collection<Likes> likes = c.getLikesReceived();
				final Collection<Chorbi> chorbies = new HashSet<Chorbi>();
				for (final Likes l : likes)
					chorbies.add(l.getLikesSender());

				result = new ModelAndView("chorbi/list");
				result.addObject("chorbies", likes);
				result.addObject("isLiker", true);
				result.addObject("uri", "chorbies/chorbi/likesList.do");
				if (a instanceof Chorbi)
					result.addObject("isAdmin", false);
				else
					result.addObject("isAdmin", true);
			}
		} else {

			final Collection<Likes> likes = c.getLikesReceived();
			final Collection<Chorbi> chorbies = new HashSet<Chorbi>();
			for (final Likes l : likes)
				chorbies.add(l.getLikesSender());

			result = new ModelAndView("chorbi/list");
			result.addObject("chorbies", likes);
			result.addObject("isLiker", true);
			result.addObject("uri", "chorbies/chorbi/likesList.do");
			result.addObject("isAdmin", true);
		}

		return result;

	}
	@SuppressWarnings("deprecation")
	private boolean fechaValida(final int mes, final int ano) {
		boolean res = false;
		final Date d = new Date();
		d.setSeconds(7 * 86400);
		if (d.getYear() + 1900 < ano)
			res = true;
		else if (d.getYear() + 1900 == ano && d.getMonth() < mes - 1)
			res = true;

		return res;
	}

	private Collection<String> getActiveBanners(final String c) {
		final Collection<String> cs = new LinkedList<String>();

		cs.addAll(Arrays.asList(c.split(",")));

		return cs;
	}

	private String bannerAleatorio(final Collection<String> activeBanners) {
		String b = "";
		final Random rnd = new Random();
		final LinkedList<String> ls = new LinkedList<String>();
		ls.addAll(activeBanners);

		final int num = (int) (rnd.nextDouble() * ls.size() + 0);
		if (ls.size() > 0)
			b = ls.get(num);
		else
			b = "ERROR";

		return b;
	}
}
