
package controllers.chorbi;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ChorbiService;
import controllers.AbstractController;
import domain.Chorbi;
import domain.CreditCard;

@Controller
@RequestMapping("/chorbi")
public class ChorbiProfileController extends AbstractController {

	@Autowired
	private ChorbiService	chorbiService;


	public ChorbiProfileController() {
		super();
	}

	@SuppressWarnings("deprecation")
	private boolean fechaValida(final int mes, final int ano) {
		boolean res = false;
		final Date d = new Date();
		d.setSeconds(7 * 86400);
		if (d.getYear() + 1900 < ano)
			res = true;
		else if (d.getYear() + 1900 == ano && d.getMonth() < mes - 1)
			res = true;

		return res;
	}

	@RequestMapping(value = "/private", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;

		final Chorbi chorbi = this.chorbiService.findByPrincipal();

		result = this.createEditModelAndView(chorbi);

		return result;
	}

	@RequestMapping(value = "/private", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Chorbi chorbi, final BindingResult binding) {

		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(chorbi);
		else
			try {

				final Chorbi le = this.chorbiService.edit(chorbi);
				result = new ModelAndView("chorbi/private");
				result.addObject("chorbi", le);
				result.addObject("message1", "actor.commit.success");
				final Collection<String> relationships = Arrays.asList("friendship", "love", "activities");
				result.addObject("relationships", relationships);

				final Collection<String> genres = Arrays.asList("man", "woman");
				result.addObject("genres", genres);
				if (chorbi.getCreditCard() != null)
					result.addObject("cc", this.ocultarNumCC(chorbi.getCreditCard().getNumber()));

			} catch (final Throwable oops) {
				result = this.createEditModelAndView2(chorbi, "actor.commit.error2");
			}
		return result;
	}

	protected ModelAndView createEditModelAndView(final Chorbi chorbi) {

		ModelAndView result;

		result = this.createEditModelAndView2(chorbi, null);

		return result;
	}

	protected ModelAndView createEditModelAndView2(final Chorbi chorbi, final String message) {
		ModelAndView result;

		result = new ModelAndView("chorbi/private");
		result.addObject("chorbi", chorbi);
		result.addObject("message", message);
		final Collection<String> relationships = Arrays.asList("friendship", "love", "activities");
		result.addObject("relationships", relationships);

		final Collection<String> genres = Arrays.asList("man", "woman");
		result.addObject("genres", genres);
		if (chorbi.getCreditCard() != null)
			result.addObject("cc", this.ocultarNumCC(chorbi.getCreditCard().getNumber()));
		return result;
	}

	// Save CCN ------------------------------------------------------------
	@RequestMapping(value = "/creditCard", method = RequestMethod.POST, params = "edit2")
	public ModelAndView save2() {
		final CreditCard cc = new CreditCard();
		cc.setExpirationMonth(0);
		cc.setExpirationYear(0);
		cc.setCodeCVV(0);
		return this.createEditModelAndView3(cc);
	}

	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/creditCard", method = RequestMethod.POST, params = "saveCreditCard")
	public ModelAndView saveCreditCard(@Valid final CreditCard creditCard, final BindingResult binding) {

		ModelAndView result;

		final Chorbi p = this.chorbiService.findByPrincipal();
		if (binding.hasErrors())
			result = this.createEditModelAndView3(creditCard);
		else if (!this.fechaValida(creditCard.getExpirationMonth(), creditCard.getExpirationYear()))
			result = this.createEditModelAndView3(creditCard, "actor.commit.error4");
		else
			try {

				p.setCreditCard(creditCard);
				final Chorbi aa = this.chorbiService.edit(p);
				result = new ModelAndView("chorbi/private");
				result.addObject("chorbi", aa);
				result.addObject("message1", "actor.commit.success");
				final Collection<String> relationships = Arrays.asList("friendship", "love", "activities");
				result.addObject("relationships", relationships);

				final Collection<String> genres = Arrays.asList("man", "woman");
				result.addObject("genres", genres);
				if (aa.getCreditCard() != null)
					result.addObject("cc", this.ocultarNumCC(aa.getCreditCard().getNumber()));

			} catch (final Throwable oops) {
				result = this.createEditModelAndView3(creditCard, "actor.commit.error");
			}

		return result;
	}

	protected ModelAndView createEditModelAndView3(final CreditCard creditCard) {
		ModelAndView result;

		result = this.createEditModelAndView3(creditCard, null);

		return result;
	}

	protected ModelAndView createEditModelAndView3(final CreditCard creditCard, final String message) {
		ModelAndView result;

		result = new ModelAndView("creditCard/edit");
		result.addObject("creditCard", creditCard);
		result.addObject("message", message);
		final Collection<String> brandNames = Arrays.asList("VISA", "MASTERCARD", "DISCOVER", "DINNERS", "AMEX");
		result.addObject("brandNames", brandNames);

		return result;
	}

	private String ocultarNumCC(final String numero) {
		final char[] res = numero.toCharArray();

		for (int i = 0; i < numero.length() - 4; i++)
			res[i] = '*';

		return String.valueOf(res);
	}

}
