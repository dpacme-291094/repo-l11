
package controllers.chorbi;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ChorbiService;
import services.EventService;
import controllers.AbstractController;
import domain.Actor;
import domain.Chorbi;
import domain.Event;

@Controller
@RequestMapping("/event/chorbi")
public class EventChorbiController extends AbstractController {

	// Services ---------------------------------------------------------------
	@Autowired
	private ChorbiService	chorbiService;
	@Autowired
	private EventService	eventService;
	@Autowired
	private ActorService	actorService;


	// Constructor ---------------------------------------------------------------
	public EventChorbiController() {
		super();
	}

	// Create -------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;

		final Chorbi c = this.chorbiService.findByPrincipal();
		final Collection<Event> events = c.getEvents();

		result = new ModelAndView("event/list");
		result.addObject("events", events);
		result.addObject("uri", "chorbi/");

		return result;

	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profile(@Valid final Integer eventId) {
		ModelAndView result;
		if (eventId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		final Event event = this.eventService.findOne(eventId);

		if (event == null || eventId == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");

		result = this.createEditModelAndView(event);
		result.addObject("event", event);

		return result;
	}
	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "register")
	public ModelAndView register(@Valid final int eventId) {
		ModelAndView result;
		final String redirectView;

		final Event event = this.eventService.findOne(eventId);
		final Chorbi c = this.chorbiService.findByPrincipal();

		if (event.getChorbies().contains(c))
			result = this.createEditModelAndView(event, "event.register.error");
		else if (event.getSeats() == 0)
			result = this.createEditModelAndView(event, "event.seats.error");
		else if (event.getMoment().before(new Date()))
			result = this.createEditModelAndView(event, "event.past.error");
		else {
			this.eventService.register(event, c);
			redirectView = "redirect: ../../../profile.do?eventId=" + eventId;
			result = new ModelAndView(redirectView);
		}
		return result;
	}
	@RequestMapping(value = "/profile", method = RequestMethod.POST, params = "unregister")
	public ModelAndView unregister(@Valid final int eventId) {
		final ModelAndView result;
		final String redirectView;
		final Event event = this.eventService.findOne(eventId);
		final Chorbi c = this.chorbiService.findByPrincipal();

		if (!event.getChorbies().contains(c))
			result = this.createEditModelAndView(event, "event.unregister.error");
		else if (event.getMoment().before(new Date()))
			result = this.createEditModelAndView(event, "event.past.error");

		else {
			this.eventService.unregister(event, c);
			redirectView = "redirect: ../../../profile.do?eventId=" + eventId;
			result = new ModelAndView(redirectView);
		}
		return result;
	}

	private ModelAndView createEditModelAndView(final Event event) {
		ModelAndView result;

		result = this.createEditModelAndView(event, null);

		return result;
	}
	protected ModelAndView createEditModelAndView(final Event event, final String message) {
		ModelAndView result;

		result = new ModelAndView("event/profile");
		result.addObject("event", event);
		result.addObject("message", message);
		result.addObject("currentDate", new Date());
		final Actor a = this.actorService.findByPrincipal();
		if (a instanceof Chorbi)
			result.addObject("ischorbi", true);
		if (event.getChorbies().contains(a))
			result.addObject("isnotregistered", false);
		else
			result.addObject("isnotregistered", true);
		return result;
	}

}
