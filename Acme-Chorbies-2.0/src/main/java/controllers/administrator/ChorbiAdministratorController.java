
package controllers.administrator;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ChorbiService;
import controllers.AbstractController;
import domain.Chorbi;

@Controller
@RequestMapping("/chorbi/administrator")
public class ChorbiAdministratorController extends AbstractController {

	// Services ---------------------------------------
	@Autowired
	private ChorbiService	chorbiService;


	// Constructors -----------------------------------
	public ChorbiAdministratorController() {
		super();
	}

	// Listing -----------------------------------

	@RequestMapping(value = "/listBan", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		final Collection<Chorbi> chorbies;
		chorbies = this.chorbiService.findAll();

		result = new ModelAndView("chorbi/listBan");
		result.addObject("chorbies", chorbies);

		return result;
	}

	@RequestMapping(value = "/fee", method = RequestMethod.GET)
	public ModelAndView payList() {
		ModelAndView result;
		final Collection<Chorbi> chorbies;
		chorbies = this.chorbiService.chorbisToPay(this.chorbiService.findAll());

		result = new ModelAndView("chorbi/fee");
		result.addObject("chorbies", chorbies);

		return result;
	}

	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/listBan", method = RequestMethod.POST, params = "ban")
	public ModelAndView ban(@Valid Chorbi chorbi) {
		ModelAndView result;
		final Collection<Chorbi> chorbies;
		chorbies = this.chorbiService.findAll();

		result = new ModelAndView("chorbi/listBan");

		try {
			if (chorbi.isBanned() == true) {
				result.addObject("message", "chorbi.ban.already");
				result.addObject("chorbies", chorbies);
			} else {
				chorbi = this.chorbiService.ban(chorbi);
				result.addObject("chorbies", chorbies);
				result.addObject("message1", "actor.commit.success");
			}
		} catch (final Throwable oops) {
			result.addObject("message", "actor.commit.error2");
			result.addObject("chorbies", chorbies);
		}

		return result;
	}

	@RequestMapping(value = "/fee", method = RequestMethod.POST, params = "pay")
	public ModelAndView pay(@Valid Chorbi chorbi) {
		ModelAndView result;
		final Collection<Chorbi> chorbies;
		chorbies = this.chorbiService.chorbisToPay(this.chorbiService.findAll());

		result = new ModelAndView("chorbi/fee");

		try {

			chorbi = this.chorbiService.pay(chorbi);
			result = new ModelAndView("redirect: ../../fee.do");
		} catch (final Throwable oops) {
			result.addObject("message", "actor.commit.error2");
		}

		return result;
	}

	@RequestMapping(value = "/listBan", method = RequestMethod.POST, params = "unban")
	public ModelAndView unban(@Valid Chorbi chorbi) {
		ModelAndView result;
		final Collection<Chorbi> chorbies;
		chorbies = this.chorbiService.findAll();

		result = new ModelAndView("chorbi/listBan");

		try {
			if (chorbi.isBanned() == false) {
				result.addObject("message", "chorbi.unban.already");
				result.addObject("chorbies", chorbies);
			} else {
				chorbi = this.chorbiService.unban(chorbi);
				result.addObject("chorbies", chorbies);
				result.addObject("message1", "actor.commit.success");
			}
		} catch (final Throwable oops) {
			result.addObject("message", "actor.commit.error2");
			result.addObject("chorbies", chorbies);
		}

		return result;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profile(@Valid final int chorbiId) {
		ModelAndView result;

		final Chorbi chorbi = this.chorbiService.findOne(chorbiId);

		if (chorbi == null)
			throw new RuntimeException("Oops! You should not be here. Go back to home page.");
		else {

			result = new ModelAndView("chorbi/profile");

			result.addObject("chorbi", chorbi);
			result.addObject("avgLikes", this.chorbiService.findStars(chorbi.getId()));
			result.addObject("age", ChorbiService.getAge(chorbi.getBirthDate()));
		}
		return result;
	}
}
