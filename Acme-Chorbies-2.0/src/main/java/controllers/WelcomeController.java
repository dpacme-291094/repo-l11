/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ChorbiService;
import services.ConfigurationService;
import domain.Actor;
import domain.Administrator;
import domain.Manager;

@Controller
@RequestMapping("/welcome")
public class WelcomeController extends AbstractController {

	@Autowired
	private ConfigurationService	configurationService;
	@Autowired
	private ActorService			actorService;
	@Autowired
	private ChorbiService			chorbiService;
	@Autowired
	private LoginService			loginService;


	// Constructors -----------------------------------------------------------

	public WelcomeController() {
		super();
	}

	// Index ------------------------------------------------------------------		

	@RequestMapping(value = "/index")
	public ModelAndView index(@RequestParam(required = false, defaultValue = "John Doe") final String name) {
		ModelAndView result = null;

		if (!LoginService.isAnonymous()) {
			final Actor a = this.actorService.findByPrincipal();
			if (a instanceof Administrator || a instanceof Manager) {

				final String banner = this.bannerAleatorio(this.getActiveBanners(this.configurationService.findAll().getBanners()));
				result = new ModelAndView("welcome/index");

				result.addObject("banner", banner);
			} else if (this.chorbiService.findByPrincipal().isBanned())
				result = new ModelAndView("redirect: ../../j_spring_security_logout");
			else if (!this.chorbiService.findByPrincipal().isBanned()) {

				final String banner = this.bannerAleatorio(this.getActiveBanners(this.configurationService.findAll().getBanners()));
				result = new ModelAndView("welcome/index");

				result.addObject("banner", banner);
			}

		} else {

			final String banner = this.bannerAleatorio(this.getActiveBanners(this.configurationService.findAll().getBanners()));
			result = new ModelAndView("welcome/index");

			result.addObject("banner", banner);
		}
		return result;
	}
	private Collection<String> getActiveBanners(final String c) {
		final Collection<String> cs = new LinkedList<String>();

		cs.addAll(Arrays.asList(c.split(",")));

		return cs;
	}

	private String bannerAleatorio(final Collection<String> activeBanners) {
		String b = "";
		final Random rnd = new Random();
		final LinkedList<String> ls = new LinkedList<String>();
		ls.addAll(activeBanners);

		final int num = (int) (rnd.nextDouble() * ls.size() + 0);
		if (ls.size() > 0)
			b = ls.get(num);
		else
			b = "ERROR";

		return b;
	}
}
