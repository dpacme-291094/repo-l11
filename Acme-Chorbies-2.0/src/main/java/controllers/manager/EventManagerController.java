
package controllers.manager;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ChirpService;
import services.ChorbiService;
import services.ConfigurationService;
import services.EventService;
import services.ManagerService;
import controllers.AbstractController;
import domain.Chirp;
import domain.CreditCard;
import domain.Event;
import domain.Manager;
import forms.FormChirp;

@Controller
@RequestMapping("/event/manager")
public class EventManagerController extends AbstractController {

	// Services ------------------------------------------------------------------
	@Autowired
	private EventService			eventService;
	@Autowired
	private ManagerService			managerService;
	@Autowired
	private ChirpService			chirpService;
	@Autowired
	private ChorbiService			chorbiService;
	@Autowired
	private ConfigurationService	configurationService;


	// Constructor ---------------------------------------------------------------
	public EventManagerController() {
		super();
	}

	// List ----------------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView result;
		result = new ModelAndView("event/list");
		final Manager m = this.managerService.findByPrincipal();
		final Collection<Event> events = this.eventService.findByManager(m.getId());

		result.addObject("events", events);
		result.addObject("uri", "manager/");

		return result;

	}

	@RequestMapping(value = "/send", method = RequestMethod.GET)
	public ModelAndView listSend() {

		ModelAndView result;
		result = new ModelAndView("event/list");
		final Manager m = this.managerService.findByPrincipal();
		final Collection<Event> events = this.eventService.findByManager(m.getId());

		result.addObject("events", events);
		result.addObject("uri", "manager/");
		return result;

	}

	@RequestMapping(value = "/modify", method = RequestMethod.GET)
	public ModelAndView listModify() {

		ModelAndView result;
		result = new ModelAndView("event/list");
		final Manager m = this.managerService.findByPrincipal();
		final Collection<Event> events = this.eventService.findByManager(m.getId());

		result.addObject("events", events);
		result.addObject("uri", "manager/");
		return result;

	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView listEdit() {

		ModelAndView result;
		result = new ModelAndView("event/list");
		final Manager m = this.managerService.findByPrincipal();
		final Collection<Event> events = this.eventService.findByManager(m.getId());

		result.addObject("events", events);
		result.addObject("uri", "manager/");
		return result;

	}

	// Create --------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;

		final Manager m = this.managerService.findByPrincipal();

		if (this.validCreditCard(m.getCreditCard())) {
			result = new ModelAndView("event/edit");
			final Event event = this.eventService.create(m);
			result.addObject("event", event);

		} else {
			result = new ModelAndView("event/list");
			final Collection<Event> events = this.eventService.findByManager(m.getId());
			result.addObject("events", events);
			result.addObject("message", "event.invalid.cc");
			result.addObject("uri", "manager/");
		}

		return result;

	}
	// Save ----------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Event event, final BindingResult binding) {
		ModelAndView result = null;

		if (binding.hasErrors()) {
			result = new ModelAndView("event/edit");
			result.addObject("event", event);
			result.addObject("message", null);
		} else
			try {

				if (!event.getMoment().after(new Date())) {
					result = new ModelAndView("event/edit");
					result.addObject("event", event);
					result.addObject("message", "event.past");
				} else {
					event = this.eventService.save(event);
					final Manager m = this.managerService.findByPrincipal();
					result = new ModelAndView("event/list");
					final Collection<Event> events = this.eventService.findByManager(m.getId());
					result.addObject("events", events);
					result.addObject("message1", "event.correct");
					result.addObject("uri", "manager/");
				}
			} catch (final Throwable oops) {
				result = new ModelAndView("event/edit");
				result.addObject("event", event);
				result.addObject("message", "error.event");
			}

		return result;
	}
	// Delete --------------------------------------------------------------
	@RequestMapping(value = "/delete", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@RequestParam final int eventId) throws Exception {
		final ModelAndView result;

		final Event event = this.eventService.findOne(eventId);
		final Manager m = this.managerService.findByPrincipal();

		this.eventService.delete(event.getId());
		result = new ModelAndView("event/list");
		final Collection<Event> events = this.eventService.findByManager(m.getId());
		result.addObject("events", events);
		result.addObject("uri", "manager/");

		return result;
	}

	// Modify --------------------------------------------------------------
	@RequestMapping(value = "/modify", method = RequestMethod.POST, params = "modify")
	public ModelAndView modify(@RequestParam final int eventId) throws Exception {

		ModelAndView result;
		result = new ModelAndView("event/edit");
		final Event event = this.eventService.findOne(eventId);

		result.addObject("event", event);

		return result;
	}

	// Metodos auxiliares --------------------------------------------------
	@SuppressWarnings("deprecation")
	public boolean validCreditCard(final CreditCard c) {
		boolean res = true;

		Date fecha = new Date();
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		fecha = calendar.getTime();

		if (c == null || ((fecha.getYear() + 1900) > c.getExpirationYear()) || (((fecha.getYear() + 1900) == c.getExpirationYear()) && (fecha.getMonth() + 1 >= c.getExpirationMonth())))
			res = false;

		return res;
	}

	// Modify --------------------------------------------------------------
	@RequestMapping(value = "/send", method = RequestMethod.POST, params = "send")
	public ModelAndView send(@RequestParam final int eventId) throws Exception {
		ModelAndView result;

		final Chirp m2 = this.chirpService.create();
		final FormChirp formChirp = new FormChirp(m2);
		formChirp.setUserRecipient("recipient");

		result = new ModelAndView("chirp/edit");
		result.addObject("notAll", true);
		result.addObject("formChirp", formChirp);
		result.addObject("create", true);
		result.addObject("eventId", eventId);
		result.addObject("uri", "event/manager/send.do");

		return result;
	}

	@RequestMapping(value = "/send", method = RequestMethod.POST, params = "save2")
	public ModelAndView send(@ModelAttribute("formChirp") final FormChirp formChirp, final Integer eventId, final BindingResult binding) throws Exception {
		ModelAndView result;

		final Chirp mensaje = this.chirpService.reconstruct(formChirp, binding);
		boolean fine = true;
		String error = "";
		if (binding.hasErrors()) {
			result = new ModelAndView("chirp/edit");

			result.addObject("notAll", true);
			result.addObject("formChirp", formChirp);
			result.addObject("create", true);
			result.addObject("eventId", eventId);
			result.addObject("message", null);
			result.addObject("uri", "event/manager/send.do");
		} else
			try {
				if (mensaje.getAttachments().length() != 0) {
					final String[] urls = mensaje.getAttachments().split(",");
					for (final String url : urls)
						if (!EventManagerController.isUrl(url)) {
							fine = false;
							error = "chirp.error.url";
							break;
						}
				}

				if (!fine) {
					result = new ModelAndView("chirp/edit");

					result.addObject("notAll", true);
					result.addObject("formChirp", formChirp);
					result.addObject("create", true);
					result.addObject("eventId", eventId);
					result.addObject("message", error);
					result.addObject("uri", "event/manager/send.do");
				} else {
					this.eventService.avisarModificacionChorbies(this.eventService.findOne(eventId), mensaje.getSubject(), mensaje.getText(), mensaje.getAttachments(), this.managerService.findByPrincipal());

					result = new ModelAndView("redirect:../../chirp/actor/outbox.do");

					result.addObject("message1", "message.commit.ok");

				}
			} catch (final Throwable oops) {
				result = new ModelAndView("chirp/edit");
				result.addObject("notAll", true);
				result.addObject("formChirp", formChirp);
				result.addObject("create", true);
				result.addObject("eventId", eventId);
				result.addObject("message", error);
				result.addObject("uri", "event/manager/send.do");

			}
		return result;
	}
	private static boolean isUrl(final String s) {
		final String regex = "(https?:\\/\\/(?:www\\.)[^\\s\\.]+\\.[^\\s]{2,}|ftp?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,}|http?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,})";

		try {
			final Pattern patt = Pattern.compile(regex);
			final Matcher matcher = patt.matcher(s);
			return matcher.matches();

		} catch (final RuntimeException e) {
			return false;
		}
	}

}
