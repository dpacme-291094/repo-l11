
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Chorbi;

@Repository
public interface ChorbiRepository extends JpaRepository<Chorbi, Integer> {

	@Query("select c from Chorbi c where c.userAccount.username like ?1")
	Chorbi findByUsername(String username);

	@Query("select avg(l.rate) from Likes l where l.likesRecipient.id= ?1")
	Double findStars(int id);

	// PART C ------------------------------
	@Query("select c from Chorbi c where c.userAccount.id = ?1")
	Chorbi findByUserAccount(int id);

	@Query("select c.coordinates.city,count(c) from Chorbi c where c.coordinates.city is not null and length(c.coordinates.city)>0 group by c.coordinates.city")
	Collection<Object> listNumberChorbiesForCity();

	@Query("select c.coordinates.country,count(c) from Chorbi c where c.coordinates.country is not null and length(c.coordinates.country)>0 group by c.coordinates.country")
	Collection<Object> listNumberChorbiesForCountry();

	@Query("select min(floor(abs((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002) * 100 + 0.5)/100.0 * sign((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002)),max(floor(abs((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002) * 100 + 0.5)/100.0 * sign((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002)),floor(abs(avg(floor(abs((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002) * 100 + 0.5)/100.0 * sign((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002))) * 100 + 0.5)/100.0 * sign(avg(floor(abs((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002) * 100 + 0.5)/100.0 * sign((sysdate()-c.birthDate)/(317.09999*365*60*60*24)+0.0002))) from Chorbi c")
	Collection<Object> calculateMinMaxAvgAgesOfChorbies();

	@Query("select sum( Case When YEAR(sysdate())>c.creditCard.expirationYear or (YEAR(sysdate())=c.creditCard.expirationYear and MONTH(sysdate())>=c.creditCard.expirationMonth) or c.creditCard is null Then 1 Else 0 End)*1.0/count(c) from Chorbi c")
	Collection<Object> calculateRatioOfChorbiesWhoHaveNotCreditCard();

	@Query("select sum( Case When c.search.relationship='activities' then 1 Else 0 end)*1.0/count(c) from Chorbi c")
	Collection<Object> calculateRatioOfChorbiesWhoSearchActivities();

	@Query("select sum( Case When c.search.relationship='friendship' then 1 Else 0 end)*1.0/count(c) from Chorbi c")
	Collection<Object> calculateRatioOfChorbiesWhoSearchFriendShip();

	@Query("select sum( Case When c.search.relationship='love' then 1 Else 0 end)*1.0/count(c) from Chorbi c")
	Collection<Object> calculateRatioOfChorbiesWhoSearchLove();

	// PART C 2.0 --------------------------

	@Query("select c from Chorbi c order by c.events.size desc")
	Collection<Chorbi> chorbiesOrderEvents();

	@Query("select c from Chorbi c")
	Collection<Chorbi> chorbiesFee();

	// PART B ------------------------------

	@Query("select c,c.likesReceived.size from Chorbi c order by c.likesReceived.size desc")
	Collection<Object> listChorbiesSortedLikes();

	@Query("select min(c.likesReceived.size),max(c.likesReceived.size),avg(c.likesReceived.size) from Chorbi c")
	Collection<Object> calculateMinMaxAvgLikesOfChorbies();

	// PART B 2.0 --------------------------

	@Query("select l.likesRecipient,l.rate*1.0 from Likes l where l.rate=(select max(l.rate) from Likes l)")
	Collection<Object> chorbiesMaxStars();

	@Query("select l.likesRecipient,l.rate*1.0 from Likes l where l.rate=(select min(l.rate) from Likes l)")
	Collection<Object> chorbiesMinStars();

	@Query("select floor(abs(avg(l.rate)) * 100 + 0.5)/100.0 * sign(avg(l.rate)) from Likes l")
	Collection<Object> chorbiesAvgStars();

	@Query("select l.likesRecipient,floor(abs(avg(l.rate)) * 100 + 0.5)/100.0 * sign(avg(l.rate)) from Likes l group by l.likesRecipient order by floor(abs(avg(l.rate)) * 100 + 0.5)/100.0 * sign(avg(l.rate)) desc")
	Collection<Object> chorbiesOrderStars();

	// PART A ------------------------------

	@Query("select min(c.chirpsReceived.size),max(c.chirpsReceived.size),avg(c.chirpsReceived.size) from Chorbi c")
	Collection<Object> minMaxAvgChirpRevieced();
	@Query("select min(c.chirpsSent.size),max(c.chirpsSent.size),avg(c.chirpsSent.size) from Chorbi c")
	Collection<Object> minMaxAvgChirpSent();

	@Query("select c,c.chirpsReceived.size from Chorbi c where c.chirpsReceived.size=(select max(c.chirpsReceived.size) from Chorbi c)")
	Collection<Object> findChorbiesWhoReceivedMoreChips();

	@Query("select c,c.chirpsSent.size from Chorbi c where c.chirpsSent.size=(select max(c.chirpsSent.size) from Chorbi c)")
	Collection<Object> findChorbiesWhoSentMoreChips();

	@Query("select c from Chorbi c where ((c.coordinates.city like ?1) and (c.coordinates.country like ?2) and (c.coordinates.state like ?3) and (c.coordinates.province like ?4) and (c.genre like ?5 )and (c.description like concat('%',?6,'%')) and (c.relationship like ?7) and (c.banned=false)) group by c")
	Collection<Chorbi> findChorbiesBySearch(String city, String country, String state, String province, String genre, String keyword, String relationship);

}
