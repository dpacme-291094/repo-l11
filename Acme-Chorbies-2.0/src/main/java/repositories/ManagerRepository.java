
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Manager;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Integer> {

	@Query("select m from Manager m where m.userAccount.username like ?1")
	Manager findByUsername(String username);

	@Query("select m from Manager m where m.userAccount.id = ?1")
	Manager findByUserAccount(int id);

	// PART C ------------------------------
	@Query("select m from Manager m order by m.events.size desc")
	Collection<Manager> managerOrderEvents();

	@Query("select m from Manager m")
	Collection<Manager> managerFee();
}
