
package repositories;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {

	@Query("select e from Event e where e.manager.id=?1")
	Collection<Event> findByManager(int id);
	@Query("select e from Event e WHERE e.moment BETWEEN ?1 AND ?2 AND e.seats > 0")
	Collection<Event> findLessOneMonth(Date sCurrentDate, Date scurrentDatePlusOneMonth);

}
