
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.Chirp;

@Repository
public interface ChirpRepository extends JpaRepository<Chirp, Integer> {

	@Query("select m from Chirp m where m.chirpSender=?1 or m.chirpRecipient=?1 order by m.moment desc")
	Collection<Chirp> findByActor(Actor actor);

	@Query("select m from Chirp m where m.chirpSender.id=?1 order by m.moment desc")
	Collection<Chirp> findBySender(int id);

	@Query("select m from Chirp m where m.chirpRecipient.id=?1 order by m.moment desc")
	Collection<Chirp> findByRecipient(int id);
}
