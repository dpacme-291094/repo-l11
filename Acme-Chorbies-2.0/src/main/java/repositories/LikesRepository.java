
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Chorbi;
import domain.Likes;

@Repository
public interface LikesRepository extends JpaRepository<Likes, Integer> {

	@Query("select m from Likes m where m.likesSender=?1 or m.likesRecipient=?1 order by m.moment desc")
	Collection<Likes> findByChorbi(Chorbi chorbi);
}
