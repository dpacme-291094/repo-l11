
package services;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ManagerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Chirp;
import domain.CreditCard;
import domain.Event;
import domain.Manager;
import forms.FormManager;

@Transactional
@Service
public class ManagerService {

	// Managed repository -------------------------------
	@Autowired
	private ManagerRepository	managerRepository;

	@Autowired
	private Validator			validator;

	// Managed services -------------------------------
	@Autowired
	private LoginService		loginService;

	@Autowired
	private ActorService		actorService;


	// Constructor --------------------------------------
	public ManagerService() {
		super();
	}

	// Simple CRUD methods ------------------------------
	public Manager create() {
		final Manager result = new Manager();
		result.setTotalFee(0.0);
		result.setEvents(new HashSet<Event>());
		result.setChirpsReceived(new HashSet<Chirp>());
		result.setChirpsSent(new HashSet<Chirp>());
		result.setCreditCard(new CreditCard());
		return result;
	}

	public Manager save(final Manager c) {
		Assert.notNull(c);
		String password;
		String hash;

		password = c.getUserAccount().getPassword();
		hash = this.encodePassword(password);
		c.getUserAccount().setPassword(hash);
		c.setCompany(this.mask(c.getCompany()));
		c.setVATNumber(this.mask(c.getVATNumber()));
		c.setName(this.mask(c.getName()));
		c.setSurname(this.mask(c.getSurname()));
		Manager res = this.managerRepository.save(c);

		res = this.managerRepository.save(res);
		return res;

	}
	public Manager edit(final Manager c) {
		Assert.notNull(c);
		c.setCompany(this.mask(c.getCompany()));
		c.setVATNumber(this.mask(c.getVATNumber()));
		c.setName(this.mask(c.getName()));
		c.setSurname(this.mask(c.getSurname()));
		return this.managerRepository.save(c);
	}
	public Manager findOne(final Integer id) {
		return this.managerRepository.findOne(id);
	}

	public Collection<Manager> findAll() {
		return this.managerRepository.findAll();
	}

	public Manager reconstruct(final FormManager formManager, final BindingResult binding) {

		final Manager result = new Manager();
		result.setId(formManager.getId());
		result.setVersion(formManager.getVersion());
		result.setName(formManager.getName());
		result.setSurname(formManager.getSurname());
		final UserAccount userAccount = formManager.getUserAccount();
		result.setUserAccount(userAccount);
		result.setPhone(formManager.getPhone());
		result.setEmail(formManager.getEmail());
		result.setCompany(formManager.getCompany());
		result.setVATNumber(formManager.getVATNumber());
		result.setTotalFee(formManager.getTotalFee());
		result.setCreditCard(formManager.getCreditCard());
		result.setEvents(formManager.getEvents());
		result.setChirpsReceived(formManager.getChirpsReceived());
		result.setChirpsSent(formManager.getChirpsSent());
		this.validator.validate(result, binding);
		return result;
	}

	public Manager reconstruct(final Manager manager, final BindingResult binding) {
		Manager result;

		if (manager.getId() == 0)
			result = manager;
		else {
			result = this.managerRepository.findOne(manager.getId());
			result.setName(manager.getName());
			result.setSurname(manager.getSurname());
			result.setEmail(manager.getName());
			result.setPhone(manager.getPhone());
			result.setCompany(manager.getCompany());
			result.setVATNumber(manager.getVATNumber());
			result.setCreditCard(manager.getCreditCard());
			result.setTotalFee(manager.getTotalFee());
			this.validator.validate(result, binding);
		}

		return result;
	}

	@SuppressWarnings("static-access")
	public Manager findByPrincipal() {
		UserAccount userAccount;
		Manager result;

		userAccount = this.loginService.getPrincipal();
		result = this.findByUserAccount(userAccount);
		return result;
	}

	public Manager findByUserAccount(final UserAccount userAccount) {
		Manager result;

		result = this.managerRepository.findByUserAccount(userAccount.getId());
		return result;

	}

	private String encodePassword(final String password) {
		Md5PasswordEncoder encoder;
		String result;

		if (password == null || "".equals(password))
			result = null;
		else {
			encoder = new Md5PasswordEncoder();
			result = encoder.encodePassword(password, null);
		}

		return result;
	}

	public void flush() {
		this.managerRepository.flush();
	}

	public FormManager createForm() {
		final FormManager formManager = new FormManager();
		final Authority a = new Authority();
		final UserAccount userAccount = new UserAccount();
		a.setAuthority("MANAGER");
		userAccount.addAuthority(a);
		formManager.setUserAccount(userAccount);
		formManager.setEvents(new HashSet<Event>());
		return formManager;
	}

	private String mask(final String text) {
		String res = "";
		try {
			final String a = text.replaceFirst("(([+])([0-9]{1,3})([ ])?)?(([0-9]{3}([ ])?[0-9]{3}([ ])?[0-9]{3})|([0-9]{3}([ ])?[0-9]{2}([ ])?[0-9]{2}([ ])?[0-9]{2}))", "***");
			res = a.replaceFirst("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", "***");
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
		}
		return res;

	}

	public Manager findByUsername(final String username) {
		return this.managerRepository.findByUsername(username);
	}

	public void checkPrincipal() {
		final Actor a = this.actorService.findByPrincipal();
		for (final Authority b : a.getUserAccount().getAuthorities())
			Assert.isTrue(b.getAuthority().equals("ADMINISTRATOR"));

	}

	public Collection<Manager> managerOrderEvents() {
		return this.managerRepository.managerOrderEvents();
	}

	public Collection<Manager> managerFee() {
		return this.managerRepository.managerFee();
	}
}
