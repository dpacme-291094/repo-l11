
package services;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.SearchTemplateRepository;
import domain.Chorbi;
import domain.SearchTemplate;

@Service
@Transactional
public class SearchTemplateService {

	@Autowired
	private SearchTemplateRepository	searchTemplateRepository;
	@Autowired
	private Validator					validator;


	public SearchTemplateService() {
		super();
	}

	public SearchTemplate create(final Chorbi c) {
		final SearchTemplate nuevo = new SearchTemplate();
		final Date currentDate = new Date();
		currentDate.setTime(currentDate.getTime() - 60000);
		nuevo.setChorbi(c);
		nuevo.setMoment(currentDate);
		nuevo.setChorbies(new HashSet<Chorbi>());
		nuevo.setGenre("both");

		return nuevo;
	}

	public SearchTemplate save(final SearchTemplate result) {
		Assert.notNull(result);
		return this.searchTemplateRepository.save(result);
	}

	public SearchTemplate edit(final SearchTemplate result) {
		Assert.notNull(result);
		return this.searchTemplateRepository.save(result);
	}

	public void delete(final int id) {
		this.searchTemplateRepository.delete(id);
	}

	public SearchTemplate findOne(final int id) {
		return this.searchTemplateRepository.findOne(id);
	}

	public Collection<SearchTemplate> findAll() {
		return this.searchTemplateRepository.findAll();
	}

	public SearchTemplate findByUser(final int id) {
		return this.searchTemplateRepository.findByChorbi(id);
	}

	public void flush() {
		this.searchTemplateRepository.flush();
	}

	public SearchTemplate reconstruct(final SearchTemplate st, final BindingResult binding) {
		SearchTemplate result;

		if (st.getId() == 0)
			result = st;
		else {
			result = this.searchTemplateRepository.findOne(st.getId());

			result.setAge(st.getAge());
			result.setCoordinates(st.getCoordinates());
			result.setGenre(st.getGenre());
			result.setKeyword(st.getKeyword());
			result.setMoment(st.getMoment());
			result.setRelationship(st.getRelationship());

			this.validator.validate(result, binding);
		}

		return result;
	}
}
