
package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.EventRepository;
import security.Authority;
import domain.Actor;
import domain.Chirp;
import domain.Chorbi;
import domain.Event;
import domain.Manager;
import forms.FormEvent;

@Transactional
@Service
public class EventService {

	// Managed repository -------------------------------
	@Autowired
	private EventRepository			eventRepository;
	@Autowired
	private ChorbiService			chorbiService;
	@Autowired
	private ConfigurationService	configurationService;
	@Autowired
	private Validator				validator;
	@Autowired
	private ActorService			actorService;
	@Autowired
	private ChirpService			chirpService;
	@Autowired
	private ManagerService			managerService;


	// Constructor --------------------------------------
	public EventService() {
		super();
	}

	// Simple CRUD methods ------------------------------
	public Event create(final Manager manager) {
		final Event result = new Event();
		final Date currentDate = new Date();
		//currentDate.setTime(currentDate.getTime() - 600000);
		result.setMoment(currentDate);
		result.setChorbies(new HashSet<Chorbi>());
		result.setManager(manager);

		return result;
	}

	public Event save(final Event e) {
		Assert.notNull(e);

		final Event event = this.eventRepository.save(e);
		final Manager m = this.managerService.findByPrincipal();

		m.setTotalFee(m.getTotalFee() + this.configurationService.findAll().getManagerFee());
		this.managerService.edit(m);

		if (event.getId() != 0)
			if (event.getChorbies().size() > 0)
				this.avisarModificacionChorbies(event, "El evento " + event.getTitle() + " fue modificado, por favor revise la informacion del evento", m);

		return event;

	}

	public void flush() {
		this.eventRepository.flush();
	}

	// Reconstruct -------------------------

	public Event reconstruct(final FormEvent formEvent, final BindingResult binding) {
		final Event result = new Event();

		result.setChorbies(formEvent.getChorbies());
		result.setDescription(formEvent.getDescription());
		result.setMoment(formEvent.getMoment());
		result.setPhoto(formEvent.getPhoto());
		result.setSeats(formEvent.getSeats());
		result.setTitle(formEvent.getTitle());
		result.setManager(formEvent.getManager());

		this.validator.validate(result, binding);
		return result;

	}

	public Event reconstruct(final Event event, final BindingResult binding) {
		Event result = new Event();

		if (event.getId() == 0)
			result = event;
		else {
			result.setChorbies(event.getChorbies());
			result.setDescription(event.getDescription());
			result.setMoment(event.getMoment());
			result.setPhoto(event.getPhoto());
			result.setSeats(event.getSeats());
			result.setTitle(event.getTitle());
			result.setManager(event.getManager());

			this.validator.validate(result, binding);
		}

		return result;
	}

	public Collection<Event> findByManager(final int manager) {
		return this.eventRepository.findByManager(manager);
	}

	public Event findOne(final int id) {
		return this.eventRepository.findOne(id);
	}

	public void delete(final int id) {

		final Manager m = this.managerService.findByPrincipal();
		final Event event = this.findOne(id);
		if (event.getChorbies().size() > 0)
			this.avisarModificacionChorbies(event, "El evento " + event.getTitle() + " fue borrado, sentimos las molestias", m);

		for (final Chorbi chorbi : event.getChorbies()) {
			chorbi.getEvents().remove(event);
			this.chorbiService.edit(chorbi);

		}
		this.eventRepository.delete(id);
	}

	public Collection<Event> findLessOneMonth() {

		final Date currentDate = new Date();
		final Calendar currentCalPlusOneMonth = Calendar.getInstance();
		currentCalPlusOneMonth.add(Calendar.DATE, 30);
		final Date currentDatePlusOneMonth = currentCalPlusOneMonth.getTime();

		return this.eventRepository.findLessOneMonth(currentDate, currentDatePlusOneMonth);

	}

	public Event findOne(final Integer id) {

		return this.eventRepository.findOne(id);
	}

	public void register(final Event event, final Chorbi chorbi) {
		Assert.isTrue(!event.getChorbies().contains(chorbi));
		Assert.isTrue(event.getSeats() > 0);
		final Collection<Chorbi> ca = event.getChorbies();
		ca.add(chorbi);
		event.setChorbies(ca);
		event.setSeats(event.getSeats() - 1);
		this.eventRepository.save(event);

		final Collection<Event> ce = chorbi.getEvents();
		ce.add(event);
		chorbi.setEvents(ce);

		this.chorbiService.edit(chorbi);

	}
	public void unregister(final Event event, final Chorbi chorbi) {
		Assert.isTrue(event.getChorbies().contains(chorbi));
		final Collection<Chorbi> ca = event.getChorbies();
		ca.remove(chorbi);
		event.setChorbies(ca);
		event.setSeats(event.getSeats() + 1);
		this.eventRepository.save(event);

		final Collection<Event> ce = chorbi.getEvents();
		ce.remove(event);
		chorbi.setEvents(ce);

		this.chorbiService.edit(chorbi);

	}

	public Collection<Event> findAll() {

		return this.eventRepository.findAll();

	}

	public void checkPrincipal() {
		final Actor a = this.actorService.findByPrincipal();
		for (final Authority b : a.getUserAccount().getAuthorities())
			Assert.isTrue(b.getAuthority().equals("ADMINISTRATOR") || b.getAuthority().equals("MANAGER") || b.getAuthority().equals("CHORBI"));

	}

	public void avisarModificacionChorbies(final Event e, final String mensaje, final Manager m) {

		for (final Chorbi c : e.getChorbies())
			this.avisoModificacion(c, mensaje, m);
	}

	public void avisoModificacion(final Chorbi c, final String mensaje, final Manager m) {

		Chirp aviso = this.chirpService.create();
		aviso.setMoment(new Date());
		aviso.setSubject("Aviso de modificacion de un evento");
		aviso.setText(mensaje);
		aviso.setUserSender(m.getUserAccount().getUsername());
		aviso.setUserRecipient(c.getUserAccount().getUsername());

		aviso.setChirpRecipient(c);
		aviso.setChirpSender(m);

		aviso = this.chirpService.save(aviso);
	}

	public void avisarModificacionChorbies(final Event e, final String subject, final String mensaje, final String attachments, final Manager m) {
		Assert.isTrue(e.getManager().equals(this.managerService.findByPrincipal()));
		for (final Chorbi c : e.getChorbies())
			this.avisoModificacion(c, subject, mensaje, attachments, m);
	}
	public void avisoModificacion(final Chorbi c, final String subject, final String mensaje, final String attachments, final Manager m) {

		Chirp aviso = this.chirpService.create();
		aviso.setMoment(new Date());
		aviso.setSubject(subject);
		aviso.setText(mensaje);
		aviso.setUserSender(m.getUserAccount().getUsername());
		aviso.setUserRecipient(c.getUserAccount().getUsername());
		aviso.setAttachments(attachments);
		aviso.setChirpRecipient(c);
		aviso.setChirpSender(m);

		aviso = this.chirpService.save(aviso);
	}
}
