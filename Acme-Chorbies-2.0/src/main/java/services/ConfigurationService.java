
package services;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ConfigurationRepository;
import domain.Configuration;

@Service
@Transactional
public class ConfigurationService {

	// Managed repository -------------------------------
	@Autowired
	private ConfigurationRepository	configurationRepository;

	@Autowired
	private Validator				validator;


	// Constructor --------------------------------------
	public ConfigurationService() {
		super();
	}

	// Simple CRUD methods ------------------------------

	public Configuration create() {
		final Configuration f = new Configuration();
		f.setBanners("");
		f.setSearchHours(":::");
		return f;
	}
	public Configuration save(final Configuration f) {
		Assert.notNull(f);

		//		if (f.getBanners().length() != 0) {
		//			final String[] urls = f.getBanners().split(",");
		//			for (final String url : urls)
		//				Assert.isTrue(ConfigurationService.isUrl(url));
		//		}

		return this.configurationRepository.save(f);
	}

	public Configuration findAll() {
		return this.configurationRepository.findAll().get(0);
	}

	// Other business methods ---------------------------

	public Configuration reconstruct(final Configuration configuration, final BindingResult binding) {
		Configuration result;

		if (configuration.getId() == 0)
			result = configuration;
		else {
			result = this.configurationRepository.findOne(configuration.getId());

			result.setBanners(configuration.getBanners());
			result.setSearchHours(configuration.getSearchHours());
			result.setChorbiFee(configuration.getChorbiFee());
			result.setManagerFee(configuration.getManagerFee());
			this.validator.validate(result, binding);
		}

		return result;
	}

	public void flush() {
		this.configurationRepository.flush();
	}

	public static boolean isUrl(final String s) {
		final String regex = "(https?:\\/\\/(?:www\\.)[^\\s\\.]+\\.[^\\s]{2,}|ftp?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,}|http?:\\/\\/[^\\s\\.]+\\.[^\\s]{2,})";

		try {
			final Pattern patt = Pattern.compile(regex);
			final Matcher matcher = patt.matcher(s);
			return matcher.matches();

		} catch (final RuntimeException e) {
			return false;
		}
	}

}
