
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Likes;

@Component
@Transactional
public class LikesToStringConverter implements Converter<Likes, String> {

	@Override
	public String convert(final Likes likes) {
		Assert.notNull(likes);
		return String.valueOf(likes.getId());
	}

}
