
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.SearchTemplate;

@Component
@Transactional
public class SearchTemplateToStringConverter implements Converter<SearchTemplate, String> {

	@Override
	public String convert(final SearchTemplate searchTemplate) {
		Assert.notNull(searchTemplate);
		return String.valueOf(searchTemplate.getId());
	}

}
