
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Chorbi;

@Component
@Transactional
public class ChorbiToStringConverter implements Converter<Chorbi, String> {

	@Override
	public String convert(final Chorbi chorbi) {
		Assert.notNull(chorbi);
		return String.valueOf(chorbi.getId());
	}

}
