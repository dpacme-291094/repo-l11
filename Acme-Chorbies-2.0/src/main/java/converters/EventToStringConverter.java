
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Event;

@Component
@Transactional
public class EventToStringConverter implements Converter<Event, String> {

	@Override
	public String convert(final Event event) {
		Assert.notNull(event);
		final String result = String.valueOf(event.getId());

		return result;
	}

}
